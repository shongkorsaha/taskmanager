<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Priority extends Model
{
    public static function showPriority(){
        return DB::table('priorities')->get();
    }
}
