<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Wedget;
use Illuminate\Support\Facades\Redirect;

class WedgetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('wedget.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('wedget.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'wedget_name' => 'required|unique:wedgets|string|max:30',

        ]);

        $wedget = Wedget::create(['wedget_name' => $request->wedget_name]);
        $wedget->save();

        return Redirect::route('wedget.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $wedget = Wedget::findOrFail($id);

        return view('wedget.show', compact('wedget'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $wedget = Wedget::findOrFail($id);

        return view('wedget.edit', compact('wedget'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'wedget_name' => 'required|string|max:40|unique:wedgets,wedget_name,' .$id

        ]);
        $wedget = Wedget::findOrFail($id);
        $wedget->update(['wedget_name' => $request->wedget_name]);


        return Redirect::route('wedget.show', ['wedget' => $wedget]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Wedget::destroy($id);

        return Redirect::route('wedget.index');
    }
}