@include('partials.header')
{{--<link href="{{ URL::asset('css/parsley.css') }}" rel="stylesheet">--}}

        <!-- page content -->
            <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            {{--<h3>Form Elements</h3>--}}
                        </div>
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    {{--<input type="text" class="form-control" placeholder="Search for...">--}}
                                    <span class="input-group-btn">
                            {{--<button class="btn btn-default" type="button">Go!</button>--}}
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Requisition Submition Form </h2>

                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    @include('partials._messages')
                                        {!! Form::open(array('url' => 'reqformsubmit','class'=>'form-horizontal form-label-left','data-parsley-validate'=>'')) !!}

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Select</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select class="form-control" name="tasktype">
                                                    <option>-- Task Type --</option>
                                                    @foreach($taskcat as $tasktype)
                                                    <option value="{{$tasktype->id}}">{{$tasktype->categorie_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            {{Form::label('taskName','Heading : *',['class'=>'control-label col-md-3 col-sm-3 col-xs-12'])}}
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::text('taskName',null,array('class'=>'form-control','required'=>'','minlength'=>'5'))}}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                                {{Form::label('detailsInput','Details *',['class'=>'control-label col-md-3 col-sm-3 col-xs-12'])}}
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                {{Form::textarea('detailsInput',null,['class'=>'form-control','rows'=>'3','placeholder'=>'Write details here.','required'=>''])}}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="exampleInputFile">File input</label>
                                           <div class="col-md-6 col-sm-6 col-xs-12">
                                               <input type="file" name="fileInput" id="exampleInputFile">
                                           </div>

                                        </div>


                                        <div class="ln_solid"></div>
                                        {{--{{ csrf_field() }}--}}
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                {{--<button type="submit" name="submit" class="btn btn-success">Submit</button>--}}
                                                {{Form::submit('Submit',['class'=>'btn btn-success'])}}

                                            </div>
                                        </div>
                                        {!! Form::close() !!}

                                </div>
                            </div>
                        </div>
                    </div>

@include('partials.formfooter')
                    {{--<script src="{{ URL::asset('js/parsley.min.js') }}"></script>--}}

