@include('partials.header')

            <!-- page content -->
            <div class="right_col" role="main">
                <div class="">
                    <div class="page-title">
                        {{--<div class="title_left">--}}
                            {{--<h3>--}}
                    {{--Invoice--}}
                    {{--<small>--}}
                        {{--Some examples to get you started--}}
                    {{--</small>--}}
                {{--</h3>--}}
                        {{--</div>--}}

                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    {{--<input type="text" class="form-control" placeholder="Search for...">--}}
                                    <span class="input-group-btn">
                            {{--<button class="btn btn-default" type="button">Go!</button>--}}
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>All Designation </h2>
                                    {{--<ul class="nav navbar-right panel_toolbox">--}}
                                        {{--<li><a href="#"><i class="fa fa-chevron-up"></i></a>--}}
                                        {{--</li>--}}
                                        {{--<li class="dropdown">--}}
                                            {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>--}}
                                            {{--<ul class="dropdown-menu" role="menu">--}}
                                                {{--<li><a href="#">Settings 1</a>--}}
                                                {{--</li>--}}
                                                {{--<li><a href="#">Settings 2</a>--}}
                                                {{--</li>--}}
                                            {{--</ul>--}}
                                        {{--</li>--}}
                                        {{--<li><a href="#"><i class="fa fa-close"></i></a>--}}
                                        {{--</li>--}}
                                    {{--</ul>--}}
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <table id="example" class="table table-striped responsive-utilities jambo_table">
                                        <thead>
                                            <tr class="headings">
                                                <th>
                                                    <input type="checkbox" class="tableflat">
                                                </th>
                                                <th>ID </th>
                                                <th>Designation </th>
                                                <th>Description </th>
                                                <th class=" no-link last"><span class="nobr">Action</span>
                                                </th>
                                            </tr>
                                        </thead>


                                        <tbody>
                                        @foreach($designations as $degtinfo)
                                            <tr class="even pointer">
                                                <td class="a-center ">
                                                    <input type="checkbox" class="tableflat">
                                                </td>
                                                <td class=" ">{{$degtinfo->id}}</td>
                                                <td class=" ">{{$degtinfo->designation_name}}</td>
                                                <td class=" ">{{$degtinfo->designation_des}}</td>
                                                <td class=" last">
                                                    <a href="{{URL::to('updatedegform')}}/{{$degtinfo->id}}"><i class="fa fa-edit green" style="font-size:20px;"></i></a>
                                                    <a href="{{URL::to('deletedeg')}}/{{$degtinfo->id}}"><i class="fa fa-trash " style="font-size:20px; color: red; margin: 0px -20px 0px 10px";></i></a>

                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>

                                    </table>
                                    {{--{{$designations->render()}}--}}
                                </div>
                            </div>
                        </div>

                        <br />
                        <br />
                        <br />

                    </div>
                </div>
@include('partials.footer')