-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 03, 2016 at 01:49 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `taskmanager`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `categorie_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `categorie_des` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `categorie_name`, `categorie_des`, `created_at`, `updated_at`) VALUES
(1, 'Network problem', 'Locan area net work is not working ', NULL, NULL),
(2, 'Hardware ', 'Hardware and accessorizes related problem', NULL, NULL),
(3, 'Email Creation', 'New email address creation', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `department_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `department_des` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `department_name`, `department_des`, `created_at`, `updated_at`) VALUES
(1, 'ICT', 'Information and Communication Technology', NULL, NULL),
(2, 'CRM', 'Credit Risk Management', NULL, NULL),
(3, 'MC', 'Monitoring and Collection ', NULL, NULL),
(4, 'ICC', 'lsjflsjf', NULL, NULL),
(5, 'ICC', 'slfjslfjslfj', NULL, NULL),
(6, 'ICC', 'LSJFLSJF', NULL, NULL),
(7, 'ICC', 'LSJFLSJF', NULL, NULL),
(8, 'ICC', 'LSJFLSJF', NULL, NULL),
(9, 'ICC', 'jjgjg', NULL, NULL),
(10, 'ICC', 'dfsfsfsf', NULL, NULL),
(11, '', '', NULL, NULL),
(12, 'Do', 'doing department', NULL, NULL),
(13, 'jflsjflsjf', 'ljfslfjlsfj', NULL, NULL),
(14, 'sfsjlfjslfjsljf', 'lksjdfljslgsjflsj', NULL, NULL),
(15, 'kd;fksf;sk', 'ldjflsjslfj', NULL, NULL),
(16, 'jfljfq', 'ldjflsjf', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `id` int(10) UNSIGNED NOT NULL,
  `designation_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `designation_des` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`id`, `designation_name`, `designation_des`, `created_at`, `updated_at`) VALUES
(1, 'Assistant Officer', 'Assistant Officer', NULL, NULL),
(2, 'Officer', 'Officer', NULL, NULL),
(3, 'Sr.Officer', 'Senior Officer ', NULL, NULL),
(4, 'Principal Officer ', 'Principal Officer ', NULL, NULL),
(5, 'Assistant Manager', 'Assistant Manager', NULL, NULL),
(6, 'Manager', 'Manager', NULL, NULL),
(7, 'Senior Manager', 'Senior Manager', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_06_25_143325_create_tasks_table', 1),
('2016_06_25_143453_create_notifications_table', 1),
('2016_06_25_143523_create_statuses_table', 1),
('2016_06_25_143544_create_categories_table', 1),
('2016_06_25_143956_create_priorities_table', 1),
('2016_06_25_144029_create_replays_table', 1),
('2016_06_25_144105_create_usertypes_table', 1),
('2016_06_25_144126_create_designations_table', 1),
('2016_06_25_144243_create_departments_table', 1),
('2016_06_25_144750_create_permissions_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `notification_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notification_des` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permission_value` int(11) NOT NULL,
  `permission_des` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `priorities`
--

CREATE TABLE `priorities` (
  `id` int(10) UNSIGNED NOT NULL,
  `prioritie_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prioritie_des` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `replays`
--

CREATE TABLE `replays` (
  `id` int(10) UNSIGNED NOT NULL,
  `replay_des` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `replay_date` datetime NOT NULL,
  `task_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `statuses`
--

CREATE TABLE `statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `statuse_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `statuse_des` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `task_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `task_des` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `task_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `task_closed_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `priority_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `task_name`, `task_des`, `photo`, `task_created_at`, `task_closed_at`, `status_id`, `user_id`, `category_id`, `priority_id`, `created_at`, `updated_at`) VALUES
(1, 'Network', 'xxvxfv', 'jpg', '2016-07-02 22:52:38', '0000-00-00 00:00:00', 0, 3, 1, 0, NULL, NULL),
(2, 'printer', 'Not printing.', 'jpg', '2016-07-02 22:49:06', '0000-00-00 00:00:00', 0, 5, 1, 0, NULL, NULL),
(3, 'dfj', 'dljfsljf', 'jpg', '2016-07-02 22:49:13', '0000-00-00 00:00:00', 0, 3, 1, 0, NULL, NULL),
(4, 'monia', 'monia', 'none', '2016-07-02 22:01:17', '0000-00-00 00:00:00', 0, 5, 1, 0, NULL, NULL),
(5, 'network problem', 'my email is not working for network disconnected. ', 'none', '2016-07-02 22:25:05', '0000-00-00 00:00:00', 0, 3, 1, 0, NULL, NULL),
(6, 'Display problem', 'Display is not working.', 'none', '2016-07-02 22:28:50', '0000-00-00 00:00:00', 0, 3, 2, 0, NULL, NULL),
(7, 'Need email', 'Need new email address.', 'none', '2016-07-02 22:29:19', '0000-00-00 00:00:00', 0, 3, 3, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `emp_id` int(11) NOT NULL,
  `full_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_type` int(11) NOT NULL,
  `user_dept` int(11) NOT NULL,
  `user_deg` int(11) NOT NULL,
  `office_cell` int(11) NOT NULL,
  `home_cell` int(11) NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `emp_id`, `full_name`, `username`, `password`, `user_type`, `user_dept`, `user_deg`, `office_cell`, `home_cell`, `address`, `photo`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'sanker.saha@diu.com', 1405621, 'Sanker Saha', 'sanker.saha', '1', 0, 0, 1, 1736102846, 1736102846, 'lsdjflsjf', 'd', NULL, NULL, NULL),
(3, 'sanker.saha1@diu.com', 0, 'Sanker Saha', 'sanker.saha', 'c4ca4238a0b923820dcc509a6f75849b', 0, 0, 0, 1736102846, 1736102846, 'dljflsjdfljf', 'jpg', NULL, NULL, NULL),
(4, 'ashikur.rahman@diu.com', 1405620, 'Ashikur Rahman', 'ashikur.rahman', 'c4ca4238a0b923820dcc509a6f75849b', 0, 0, 0, 1736102846, 1736102846, 'Address ', 'jpg', NULL, NULL, NULL),
(5, 'monia.tanha@diu.com', 1405629, 'Monia Tanha', 'monia.tanha', 'c4ca4238a0b923820dcc509a6f75849b', 0, 1, 2, 1736102846, 1736102846, 'ajdlfj', 'jpg', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `usertypes`
--

CREATE TABLE `usertypes` (
  `id` int(10) UNSIGNED NOT NULL,
  `usertype_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `usertype_des` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `priorities`
--
ALTER TABLE `priorities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `replays`
--
ALTER TABLE `replays`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tasks_status_id_index` (`status_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_emp_id_unique` (`emp_id`);

--
-- Indexes for table `usertypes`
--
ALTER TABLE `usertypes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `priorities`
--
ALTER TABLE `priorities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `replays`
--
ALTER TABLE `replays`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `statuses`
--
ALTER TABLE `statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `usertypes`
--
ALTER TABLE `usertypes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
