<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Category;
use App\Http\Requests;
use App\Task;
use App\Priority;
use DB;
use Session;


class TaskController extends Controller
{

    public $user;
    function __construct(Request $request)
    {
        if(!$request->session()->has('logininfo')){
            return redirect('/')->with('error','You have to Login first');
        }

      $data =  $request->session()->get('logininfo');
      $this->user = $data[0];

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $taskcategory = Category::showtaskcategory();

        return view('requisition.reqform',['user'=>$this->user, 'taskcat'=>$taskcategory]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request, array(
            'tasktype'      => 'required |max:255',
            'taskName'      => 'required |max:255',
            'detailsInput'  => 'required |max:255',
    ));
        $data = $request->all();
        //dd($data);


       // echo $data=['userid']=$this->user->id;

        $data['id'] = $this->user->id;
        $data['user_dept']= $this->user->user_dept;
        //dd($data);
         if($request->hasFile('fileInput')){
              $data['ext'] = $request->file('fileInput')->getClientOriginalExtension();

         }
        else{
            $data['ext'] = 'none';
        }

        $photo    = $request->file('fileInput');

        if(in_array($data['ext'], array('jpg','png','gif','jpeg','none'))){

          $id     = Task::store($data);
            if($id > 0){
              $path = public_path().'/files';
                if($data['ext'] != 'none'){
                    $photo->move($path,$id.'.'.$data['ext']);
                }
            }
        }
        Session::flash('success','Your request has been submitted successfully!');
       return redirect('\reqform');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {


        $tasklist = Task::showreq($this->user);

        //dd($this->user->user_dept);
        //dd($this->user);
      // dd($tasklist);
       // $getcatnamemethod = Tadk::getcatname();
        return view('requisition.showreq',['user'=>$this->user,'requestlist'=>$tasklist]);
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     *
     */
    public function update(Request $request)
    {
        $id = $request->id;
        $taskrow = Task::findTaskRowNumber($id);
        $taskcategory = Category::showtaskcategory();
        $priorityvar = Priority::showPriority();
        $ITuserlit  = Task::userlist();
        //dd($ITuserlit);
        return view('requisition.updatereqform',['user'=>$this->user,'taskcat'=>$taskcategory ,'prioritys'=>$priorityvar,'viewid'=>$taskrow[0],'ITuser'=>$ITuserlit]);
    }

    public function updatesave(Request $request){
        $data= $request->all();
          //dd($data);
        $data['userid'] = $this->user->id;
       $updateTaskdata = Task::updateTask($data);
        //dd($data);
        return redirect('allreq')->with('success','updated successfully.');
    }



//    public function updatesave(){
//
//        return redirect('\reqformupdate');
//    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
