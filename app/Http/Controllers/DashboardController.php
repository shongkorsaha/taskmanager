<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Task;
use App\User;

class DashboardController extends Controller
{
    //
    public $user;
    function __construct(Request $request)
    {
        if(!$request->session()->has('logininfo')){
            return redirect('/')->with('error','Direct Access is not Permitted. Kindly Login!');
        }
        $userdata = $request->session()->get('logininfo');
        $this->user = $userdata[0];
        //dd($this->user);

    }

    public function index(Request $request){

        $allTaskList = Task::allTask();
        $userList = User::UserListAsAdmin();

        $data = Task::paiChartTaskDoneByCat();
        //dd($data);


        // dd($userList);

     return view('dashboard.home',['user'=>$this->user, 'tasks'=>$allTaskList, 'individual'=>$userList, 'paiChartData'=>$data]);
    }

    public function showalltask(){
//        $allTaskList = Task::allTask();

//        dd($allTaskList);

        //return view ('dashboard.home',['user'=>$this->user]);
    }

    public function DashboradElement(Request $request){
        $allTaskList = Task::allTask();
      //  dd($allTaskList);

        if($request->ajax()){
        $allTaskList = Task::allTask();
        //dd($allTaskList);
            echo  json_encode($allTaskList) ;
        }
        else{
            echo "<strong> INVALID REQUEST </strong>";
        }
    }



}
