@include('partials.header')

        <!-- page content -->
<div class="right_col" role="main">
    <div class="">

        <div class="page-title">
            <div class="title_left">

            </div>
            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        {{--<input type="text" class="form-control" placeholder="Search for...">--}}
                                    <span class="input-group-btn">
                            {{--<button class="btn btn-default" type="button">Go!</button>--}}
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Update Profile </h2>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />

                        <form action="{{URL::to('updateuser')}}/{{$profile->id}}" method="post" enctype="multipart/form-data" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Employee ID : <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="hidden" class="form-control" name="id" value="{{$profile->id}}">
                                    <input type="text" class="form-control" name="empid" value="{{ $profile->emp_id }}" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Full Name : <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" disabled class="form-control" value="{{ $profile->full_name }}" name="fname"  >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Email : <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" name="email"  value="{{ $profile->email }}" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Username : <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" disabled  class="form-control " name="username" value="{{ $profile->username }}" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Password : <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text"   class="form-control " name="password" value="" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Confirm Password : <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text"   class="form-control " name="confPassword" value="" >
                                </div>
                            </div>



                            <div class="form-group">

                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Department : <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select class="form-control"  name="dept" required>
                                        <option > -- Select Department --</option>

                                        @foreach($dept as $d)
                                            <option
                                                    @if($d->id == $profile->user_dept)
                                                    selected
                                                    @endif
                                                    value="{{$d->id}}">{{$d->department_name}}
                                            </option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                {{--{{print_r($profile)}}--}}
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Designation : <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select class="form-control"  name="deg" required>
                                        <option> -- Select Designation --</option>

                                        @foreach($deg as $degination)
                                            <option
                                                    @if($degination->id == $profile->user_deg)
                                                        selected
                                                    @endif
                                                    value="{{$degination->id}}">{{$degination->designation_name}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Access Level : <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select class="form-control"  name="usertype" required>
                                        <option> -- Select Access Level --</option>
                                        @foreach($utype as $accellLevel)
                                            <option
                                                    @if($accellLevel->access_id == $profile->user_type )
                                                            selected
                                                    @endif
                                                    value="{{$accellLevel->access_id}}">{{$accellLevel->access_level}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Office Phone : <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" disabled class="form-control" name="fphone" value="{{ $profile->office_cell }}" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Home Phone : <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" disabled class="form-control" name="hphone" value="{{ $profile->home_cell }}" >
                                </div>
                            </div>

                            <div class="ln_solid"></div>
                            {{ csrf_field() }}
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    {{--<button type="submit" class="btn btn-primary">Cancel</button>--}}
                                    <button type="submit" name="updateUser" class="btn btn-success">Update</button>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>

@include('partials.footerForm')