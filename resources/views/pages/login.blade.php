<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>:: E-TaskManagementSystem :: </title>

    <!-- Bootstrap core CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="css/custom.css" rel="stylesheet">
    <link href="css/icheck/flat/green.css" rel="stylesheet">


    <script src="js/jquery.min.js"></script>

    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>

<body style="background:url('images/login-bg.png') repeat center;">

    <div class="">
        <a class="hiddenanchor" id="toregister"></a>

        <a class="hiddenanchor" id="tologin"></a>

        <div id="wrapper">
            @include('partials._messages')
            <div id="login" class="animate form">
                <section class="login_content">

                    <form action="{{ URL::to('login') }}" method="post" >
                        <h1>Login Form</h1>

                        <div>
                            <input type="text" class="form-control" name="username" placeholder="Username" required="" />
                        </div>
                        <div>
                            <input type="password" class="form-control" name="password" placeholder="Password" required="" />
                        </div>
                        <div>
                            {{ csrf_field() }}
                           <button type="submit" class="btn green" name="submit">Log in</button>
                           <p class="change_link pull-left">New to site?
                                <a href="#toregister" class="to_register"> Create Account </a>
                            </p>
                            <a class="reset_pass pull-right" href="forgetpassword"> Forgot password?</a>
                        </div>
                        <div class="clearfix"></div>
                        <div class="separator">

                            <div>
                                <h1><i class="fa fa-paw" style="font-size: 26px;"></i> E-TaskManagementSystem</h1>
                            </div>
                        </div>
                    </form>
                    <!-- form -->
                </section>
                <!-- content -->
            </div>



            <div id="register" class="animate form">
                <section class="login_content">
                    <form action="{{ URL::to('register') }}" method="post" enctype="multipart/form-data">
                        <h1>Create Account</h1>
                        <div>
                            <input type="text" class="form-control" name="empid" placeholder="Employee ID" required="" />
                        </div>
                        <div>
                            <input type="text" class="form-control" name="fname" placeholder="Full Name" required="" />
                        </div>
                        <div>
                            <input type="email" class="form-control" name="email" placeholder="Email" required="" />
                        </div>
                        <div>
                            <input type="text" class="form-control" name="username" placeholder="Username" required="" />
                        </div>
                        <div>
                            <input type="password" class="form-control" name="password" placeholder="Password" required="" />
                        </div>
                        <div>
                            <input type="password" class="form-control" name="retype" placeholder="Confirm Password" required="" />
                        </div>

                        <div>
                            <select class="form-control"  name="dept" required>
                                <option> -- Select Department --</option>
                                @foreach($dept as $d)
                                <option value="{{$d->id}}">{{$d->department_name}}</option>
                                @endforeach

                            </select>
                        </div>
                        <br />

                        <div>
                           <select class="form-control"  name="deg" required>
                               <option> -- Select Designation --</option>
                               @foreach($designation as $deg)
                               <option value="{{$deg->id}}">{{$deg->designation_name}}</option>
                               @endforeach
                           </select>
                        </div>
                        <br />

                        <input type="hidden" class="form-control" name="usertype"  required="" value="0" />

                        <div>
                            <input type="text" class="form-control" name="fphone" placeholder="Office Phone" required="" />
                        </div>
                        <div>
                            <input type="text" class="form-control" name="hphone" placeholder="Home Phone" required="" />
                        </div>
                        <div>
                            <textarea name="address" class="form-control" rows="3" placeholder="Address" > </textarea>

                        </div>
                        <div>
                          <label style="text-align: left; font-weight: bold;">
                              Upload Photo:   <input type="file" class="form-control" name="photo" placeholder="Home Phone" required="" />
                           </label>
                        </div>
                        <div>
                            {{ csrf_field() }}
                            <button type="submit" class="btn green" name="submit">Create Account</button>
                        </div>
                        <div class="clearfix"></div>
                        <div class="separator">

                            {{--<p class="change_link">Already a member ?--}}

                            </p>
                            <div class="clearfix"></div>
                            
                        </div>
                    </form>
                    <!-- form -->
                </section>
                <!-- content -->
            </div>
        </div>
    </div>

</body>

</html>
