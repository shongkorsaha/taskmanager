<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;


class Category extends Model
{
    public static function showtaskcategory(){
        return $data = DB::table('categories')->get();


    }

    public static function getcatname($id){
        $result = DB::table('categories')->select('categorie_name')->where('id', $id);
        return $result->categorie_name;
    }

    public static function storeCategory($data){
        return   DB::table('categories')->insertGetId(
            [
                'categorie_name'     => $data['catName'],
                'categorie_des'      => $data['catDetails'],
                'dept_id'            => $data['catDept']

            ]

        );
    }

    public static function showCategory(){
        return DB::table('categories')
            ->join('departments','departments.id' ,'=', 'categories.dept_id')
            ->select('categories.id as categoriesid',
                     'categories.categorie_name as categorie_name',
                     'categories.categorie_des as categorie_des',
                     'departments.department_name as categoriDepartment')->paginate(5);
    }

    public static function showCategoryById($id){
        return DB::table('categories')
            ->where('id',$id)
            ->get();
    }

    public static function updateCategory($data){
        return   DB::table('categories')
            ->where('id', $data['id'])
            ->update([
                'categorie_name'     => $data['categorie_name'],
                'categorie_des'      => $data['categorie_des'],
                'dept_id'            => $data['dept_id']]);
    }

    public static function destroyCategory($request)
    {
        return DB::table('categories')
            ->where('id', $request->id)
            ->delete();
    }
}
