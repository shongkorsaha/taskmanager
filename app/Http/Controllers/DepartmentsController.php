<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Department;


class DepartmentsController extends Controller
{
    public $user;
    function __construct(Request $request)
    {
        if(!$request->session()->has('logininfo')){
            return redirect('/')->with('error','You have to Login first');
        }

        $data =  $request->session()->get('logininfo');
        $this->user = $data[0];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dept.deptadd',['user'=>$this->user]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        //dd($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'deptName'=>'required',
            'deptDetails'=>'required|max:255',
        ]);
        $data        = $request->all();
        $departments = Department::store($data);

      return redirect('newdept')->with('success','Your request has been submitted successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $dep = Department::dbshow();
        return view('dept.showdept',['user'=>$this->user,'deptmants'=>$dep]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editform($id)
    {
        $dept = Department::showDeptById($id);
        //dd($dept);
        //$dep = User::showDept();
//        dd($cat);
        return view('dept.updatedeptform',['user'=>$this->user, 'dept'=>$dept[0]]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->all();
        $update =Department::updateDept($data);
        return redirect('alldept' )->with('success','Data Updated Successfully!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $data = $request->all();
//        dd($data);
        $deleteDept = Department::destroyDeptById($request);
        return redirect('alldept')->with('success','Delete successfully.');
    }
}
