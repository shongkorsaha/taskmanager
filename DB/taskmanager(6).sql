-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 14, 2016 at 09:31 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `taskmanager`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `categorie_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `categorie_des` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dept_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `categorie_name`, `categorie_des`, `dept_id`, `created_at`, `updated_at`) VALUES
(1, 'IT Asset Transfer', 'All IT asset related transfer', 1, NULL, NULL),
(2, 'Email Address Creation', 'Email address creation for new joiner. ', 2, NULL, NULL),
(3, 'Prime Access', 'User name and password create', 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `department_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `department_des` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `department_name`, `department_des`, `created_at`, `updated_at`) VALUES
(1, 'Maintenance and Support', 'maintenance and support', NULL, NULL),
(2, 'Network Infrastructure', 'Network Infrastructure', NULL, NULL),
(3, 'Network Security', 'Network Security', NULL, NULL),
(4, 'DBA', 'Database Administrator', NULL, NULL),
(5, 'Call Center', 'Call Center 24X7 open.', NULL, NULL),
(6, 'CRM', 'Credit Risk Management. ', NULL, NULL),
(7, 'ICC', 'ICC', NULL, NULL),
(8, 'account and Finance ', 'account and Finance ', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `id` int(10) UNSIGNED NOT NULL,
  `designation_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `designation_des` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`id`, `designation_name`, `designation_des`, `created_at`, `updated_at`) VALUES
(1, 'Assistant Officer', 'Assistant Officer', NULL, NULL),
(2, 'Officer', 'Officer', NULL, NULL),
(3, 'Sr.Officer', 'Senior Officer ', NULL, NULL),
(4, 'Principal Officer ', 'Principal Officer ', NULL, NULL),
(5, 'Assistant Manager', 'Assistant Manager', NULL, NULL),
(6, 'Manager', 'Manager', NULL, NULL),
(7, 'Senior Manager', 'Senior Manager', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_06_25_143325_create_tasks_table', 1),
('2016_06_25_143453_create_notifications_table', 1),
('2016_06_25_143523_create_statuses_table', 1),
('2016_06_25_143544_create_categories_table', 1),
('2016_06_25_143956_create_priorities_table', 1),
('2016_06_25_144029_create_replays_table', 1),
('2016_06_25_144105_create_usertypes_table', 1),
('2016_06_25_144126_create_designations_table', 1),
('2016_06_25_144243_create_departments_table', 1),
('2016_06_25_144750_create_permissions_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `notification_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notification_des` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permission_value` int(11) NOT NULL,
  `permission_des` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `priorities`
--

CREATE TABLE `priorities` (
  `id` int(10) UNSIGNED NOT NULL,
  `prioritie_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prioritie_des` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `priorities`
--

INSERT INTO `priorities` (`id`, `prioritie_name`, `prioritie_des`, `created_at`, `updated_at`) VALUES
(1, 'Low', 'In general', NULL, NULL),
(2, 'Medium', 'Medium', NULL, NULL),
(3, 'High', 'Emergency ', NULL, NULL),
(4, 'Extra High', 'Extra High', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `replays`
--

CREATE TABLE `replays` (
  `id` int(10) UNSIGNED NOT NULL,
  `replay_des` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `replay_date` datetime NOT NULL,
  `task_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `statuses`
--

CREATE TABLE `statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `statuse_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `statuse_des` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `statuses`
--

INSERT INTO `statuses` (`id`, `statuse_name`, `statuse_des`, `created_at`, `updated_at`) VALUES
(0, 'Pending', 'pending', NULL, NULL),
(1, 'Authorized', 'authorized by dept head', NULL, NULL),
(2, 'Approved', 'approved by head of IT or HR or DMD or MD', NULL, NULL),
(3, 'Deny', 'deny', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `task_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `task_des` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `task_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `task_closed_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_dept` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `priority_id` int(11) NOT NULL,
  `task_updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `task_name`, `task_des`, `photo`, `task_created_at`, `task_closed_at`, `status_id`, `user_id`, `user_dept`, `category_id`, `priority_id`, `task_updated_at`, `updated_by`) VALUES
(10, 'Laptop ', 'up', 'none', '2016-07-14 18:43:35', '0000-00-00 00:00:00', 2, 6, 0, 1, 1, '0000-00-00 00:00:00', 6),
(11, 'User name password', 'prime e user name lagbe', 'none', '2016-07-04 21:36:42', '0000-00-00 00:00:00', 0, 5, 0, 3, 2, '0000-00-00 00:00:00', 5),
(12, 'Laptop Transfer', 'My laptop needs to transfer to IT.', 'none', '2016-07-09 17:45:11', '0000-00-00 00:00:00', 0, 7, 0, 1, 1, '0000-00-00 00:00:00', 0),
(13, 'New email', 'I need new email', 'none', '2016-07-13 16:10:07', '0000-00-00 00:00:00', 0, 6, 0, 2, 1, '0000-00-00 00:00:00', 0),
(14, 'Prime needed.', 'update Prime needed.', 'none', '2016-07-13 18:23:51', '0000-00-00 00:00:00', 0, 12, 6, 3, 2, '0000-00-00 00:00:00', 12),
(15, 'Printer', '1 Printer to IT', 'none', '2016-07-14 18:39:57', '0000-00-00 00:00:00', 2, 12, 0, 1, 1, '0000-00-00 00:00:00', 12),
(16, 'email', 'slflsjflsj', 'none', '2016-07-14 19:28:37', '0000-00-00 00:00:00', 1, 14, 0, 2, 1, '0000-00-00 00:00:00', 0),
(17, 'Prime needed.', 'prime', 'none', '2016-07-14 18:44:04', '0000-00-00 00:00:00', 2, 14, 6, 3, 1, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `emp_id` int(11) NOT NULL,
  `full_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_type` int(11) NOT NULL,
  `user_dept` int(11) NOT NULL,
  `user_deg` int(11) NOT NULL,
  `office_cell` int(11) NOT NULL,
  `home_cell` int(11) NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `emp_id`, `full_name`, `username`, `password`, `user_type`, `user_dept`, `user_deg`, `office_cell`, `home_cell`, `address`, `photo`, `remember_token`, `created_at`, `updated_at`) VALUES
(5, 'monia.tanha@diu.com', 1405629, 'Monia Tanha', 'monia.tanha', 'c4ca4238a0b923820dcc509a6f75849b', 2, 1, 2, 1736102846, 1736102846, 'ajdlfj', 'jpg', NULL, NULL, NULL),
(6, 'sanker.saha@diu.com', 1405621, 'Sanker Saha', 'sanker.saha', 'c4ca4238a0b923820dcc509a6f75849b', 3, 4, 2, 1736102846, 1736102846, 'Mirpur 14', 'jpg', NULL, NULL, NULL),
(7, 'ashikur.rahman@diu.com', 1405627, 'Ashikur Rahman', 'ashikur.rahman', 'c4ca4238a0b923820dcc509a6f75849b', 2, 4, 2, 1719118833, 1719118833, 'Tangail', 'jpg', NULL, NULL, NULL),
(8, 'mahmudul.tuhin@diu.com', 1405721, 'Mahmudul Hasan Tuhin', 'mahmudul.tuhin', 'c4ca4238a0b923820dcc509a6f75849b', 3, 2, 5, 1671067476, 1671067476, 'DIU', 'jpg', NULL, NULL, NULL),
(12, 'test@gmail.com', 1505627, 'test', 'test', 'c4ca4238a0b923820dcc509a6f75849b', 0, 6, 1, 1736102846, 1736102846, 'ldsjlfj', 'jpg', NULL, NULL, NULL),
(13, 'bishwanath.saha@diu.com', 16061000, 'Bishwanath Saha', 'bishwanath.saha', 'c4ca4238a0b923820dcc509a6f75849b', 1, 6, 5, 1736102846, 1736102846, 'ST level 3', 'jpg', NULL, NULL, NULL),
(14, 'barsha.saha@diu.com', 16061001, 'Barsha Saha', 'barsha.saha', 'c4ca4238a0b923820dcc509a6f75849b', 0, 6, 1, 1736102846, 1736102846, 'ST 05', 'jpg', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `usertypes`
--

CREATE TABLE `usertypes` (
  `access_id` int(10) UNSIGNED NOT NULL,
  `access_level` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `usertypes`
--

INSERT INTO `usertypes` (`access_id`, `access_level`, `created_at`, `updated_at`) VALUES
(0, 'Normal', NULL, NULL),
(1, 'Moderator', NULL, NULL),
(2, 'Admin', NULL, NULL),
(3, 'Supper Admin', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `priorities`
--
ALTER TABLE `priorities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `replays`
--
ALTER TABLE `replays`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tasks_status_id_index` (`status_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_emp_id_unique` (`emp_id`);

--
-- Indexes for table `usertypes`
--
ALTER TABLE `usertypes`
  ADD PRIMARY KEY (`access_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `priorities`
--
ALTER TABLE `priorities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `replays`
--
ALTER TABLE `replays`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `statuses`
--
ALTER TABLE `statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `usertypes`
--
ALTER TABLE `usertypes`
  MODIFY `access_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
