<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->integer('emp_id')->unique();
            $table->string('full_name');
            $table->string('username');
            $table->string('password');
            $table->integer('user_type');
            $table->integer('user_dept');
            $table->integer('user_deg');
            $table->integer('office_cell');
            $table->integer('home_cell');
            $table->string('address');
            $table->string('photo');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
