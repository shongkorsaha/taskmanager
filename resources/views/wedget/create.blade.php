@extends('layouts.master')

@section('title')

    <title>Create a Wedget</title>

@endsection

@section('content')

        <ol class='breadcrumb'><li><a href='/'>Home</a></li><li><a href='/wedget'>Wedgets</a></li><li class='active'>Create</li></ol>

        <h2>Create a New Wedget</h2>

        <hr/>


        <form class="form" role="form" method="POST" action="{{ url('/wedget') }}">

        {!! csrf_field() !!}

        <!-- wedget_name Form Input -->
            <div class="form-group{{ $errors->has('wedget_name') ? ' has-error' : '' }}">
                <label class="control-label">Wedget Name</label>

                    <input type="text" class="form-control" name="wedget_name" value="{{ old('wedget_name') }}">

                    @if ($errors->has('wedget_name'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('wedget_name') }}</strong>
                                    </span>
                    @endif

            </div>


            <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg">
                        Create
                    </button>
            </div>

        </form>

@endsection