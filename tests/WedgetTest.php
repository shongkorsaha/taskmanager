<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class WedgetTest extends TestCase
{
    public function testCreateNewWedget()
    {

        $randomString = str_random(10);

        $this->visit('/wedget/create')
              ->type($randomString, 'wedget_name')
              ->press('Create')
              ->seePageIs('/wedget');
    }
}