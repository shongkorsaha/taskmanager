<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Excel;
use App\Task;


class ExcelController extends Controller

{
    public $user;
    function __construct(Request $request)
    {
        if(!$request->session()->has('logininfo')){
            return redirect('/')->with('error','You have to Login first');
        }

        $data =  $request->session()->get('logininfo');
        $this->user = $data[0];

    }

    public function getExport(){
        $exporTallDoneTaskLis = Task::showreq($this->user);
        //dd($exporTallDoneTaskLis);
        foreach ($exporTallDoneTaskLis as $tasks)
        {
            $data[] = array(
                "Requisition ID"            => $tasks->taskid,
                "Requisition Name"    => $tasks->categorie_name,
                "Description"          => $tasks->task_des,
                "Requisition By"             => $tasks->email,
                "Requisition Status"            => $tasks->status,
            );
        }

        Excel::create('All Report', function($excel) use($data){
            $excel->sheet('Sheet 1',function($sheet)use($data){
                $sheet->fromArray($data);
                $sheet->row(1, function($row) {

                    // call cell manipulation methods
                    $row->setBackground('#4d94ff');
                });
                $sheet->setHeight(1,20);
            });
        })->export('xlsx');
    }


    public function getExportIndividual(Request $request )
    {
        $request->all();
        //dd($request);
        $individualTask = Task::individualDoneTask($request, $this->user);
        //dd($individualTask);
        foreach ($individualTask as $tasks) {
            $data[] = array(
                "SN" => $tasks->taskid,
                "Task Name" => $tasks->categorie_name,
                "Task Des" => $tasks->task_des,
                "Assigned To" => $tasks->full_name,
                "Status" => $tasks->STATUS,
            );
        }

        @Excel::create('Export Data', function ($excel) use ($data) {
            $excel->sheet('Sheet 1', function ($sheet) use ($data) {
                $sheet->fromArray($data);
                $sheet->row(1, function ($row) {

                    // call cell manipulation methods
                    $row->setBackground('#4d94ff');

                });
                $sheet->setHeight(1, 20);
            });
        })->export('xlsx');
    }



}
