<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Department;
use App\Designation;

class UserController extends Controller
{
    public $user;
    function __construct(Request $request)
    {
        if (!$request->session()->has('logininfo')) {
            return redirect('/')->with('error', 'You have to Login first');
        }

        $data = $request->session()->get('logininfo');
        $this->user = $data[0];
    }
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $userlist = User::showUserList($this->user->id,$this->user->user_type);
       // dd($userlist);
        return view('user.showuser',['user'=>$this->user, 'alluser'=>$userlist]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function showUserCreateForm(){
        $dep = Department::dbshow();
        $deg = Designation::showdeg();
        $userType = User::showUserType();
        return view('user.createuserform',['user'=>$this->user, 'dept'=>$dep, 'degi'=>$deg, 'uType'=>$userType] );

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'empid'     => 'required |max:255',
            'fname'     => 'required |max:255',
            'email'     => 'required |max:255',
            'username'  => 'required |max:255',
            'password'  => 'required |max:255',
            'retype'    => 'required |max:255',
            'dept'      => 'required |max:255',
            'deg'       => 'required |max:255',
            'usertype'  => 'required |max:255',
            'fphone'    => 'required |max:255',
            'hphone'    => 'required |max:255',
            'address'   => 'required |max:255',
            'photo'     => 'required |max:255',

        ));

        $data = $request->all();
        $file = $request->file('photo')->getClientOriginalExtension();
        $photo = $request->file('photo');
        $checker = $this->validity($data,$file);
        //dd($checker);
        if($checker['status'] === true){
            $id =  User::CreateAccount($data,$file);
            if($id > 0){

                $path = public_path().'/photos';

                if($photo->move($path,$id.'.'.$file)){

                    //    $request->session()->put('logininfo',User::where('id',$id)->get());
                    //    $sess = $request->session()->get('logininfo');
                    //    dd($sess);
                    return redirect($checker['redirect'])->with('success',$checker['sms']);

                }
                else{

                    $this->destroy($id);
                }
            }
        }
        elseif($checker['status'] === false){
            return redirect($checker['redirect'])->with('error',$checker['sms']);
        }

    }
    public function validity($data,$file){
        unset($data['submit']);
        $validity =  array();
        $fext = array('png','jpg','gif','jpeg');

        if(in_array('',$data) || in_array(NULL,$data) || empty($data) || $data['password'] != $data['retype']){

            $validity['status'] = false;
            if(!in_array($file,$fext)){
                $validity['sms'] = "All Fields are required & Invalid File format";
            }
            else{
                $validity['sms'] = "All Fields are required ";
            }
            $validity['redirect'] = 'newuserform';
        }
        else{
            $validity['status'] = true;
            $validity['sms'] =  'User created successfully';
            $validity['data'] = $data;
            $validity['redirect'] = 'newuserform';
        }


        $validity['file']=$file;
        return $validity;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showUserUpdateForm(Request $request)
    {
        $udata = User::showUserInUpdateForm($request->id);
        $dept  = User::showDept();
        $deg   = User::showDesignations();
        $uType = User::showUserType();

//         dd($udata);
       // dd($dept);
        //dd($deg);
        //dd($uType);
        return view('user.updateuserform',['user'=>$this->user,'profile'=>$udata[0],'dept'=>$dept,'deg'=>$deg,'utype'=>$uType]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data   = $request->all();
        //dd($data);
        if($data['password']==$data['confPassword']){
            $update = User::updateUserAccess($data);
            return redirect('updateuserform/'.$data['id'] )->with('success','Data Updated Successfully!');
        }
        else{
            return redirect('updateuserform/'.$data['id'] )->with('error','message');
        }

//      dd($data);
//      dd($update);
     // return redirect('updateuserform/'.$data['id'] )->with('success','Data Updated Successfully!');
        //return redirect('alluser');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
