<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use DB;
use App\Department;
use App\Designation;
use Session;
class welcomeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response.
     */
    public function index(Request $request)
    {

        $sms='';
        $data = $request->session()->all();
        if($request->session()->has('success')){
            $sms['success'] = $request->session()->get('success');
        }
        if($request->session()->has('error')){
            $sms['error'] = $request->session()->get('error');
        }
        $dep = Department::dbshow();
        $deg = Designation::showdeg();
        //dd($deg);

        return view('pages.login',['sms'=>$sms,'dept'=> $dep,'designation'=>$deg ]);

    }

    /**
     * @param Request $request
     * login
     */
    public function login(Request $request){

        $data       = $request->all();
        $username   = $data['username'];
        $password   = md5($data['password']);
        $users      = User::join('usertypes', 'usertypes.access_id' ,'=', 'users.user_type')
                    ->where(['username'=>$username,'password'=>$password])->get();
        $total      = count($users);

        if($total == 1){
          $request->session()->put('logininfo', $users);
          return  redirect('dashboard')->with('success','Logged In Successfully');

        }else{
            return  redirect('/')->with('error','Wrong Username & Password');
        }
    }

    public function logout(Request $request){

        $request->session()->flash('logininfo');
        return redirect('/');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * register
     */
    public function register(Request $request){

        $data = $request->all();
        //dd($data);
        $file = $request->file('photo')->getClientOriginalExtension();
        $photo = $request->file('photo');
        $checker = $this->validity($data,$file);
        //dd($checker);
        if($checker['status'] === true){
          $id =  User::CreateAccount($data,$file);
            if($id > 0){

                $path = public_path().'/photos';

                if($photo->move($path,$id.'.'.$file)){

                //    $request->session()->put('logininfo',User::where('id',$id)->get());
               //    $sess = $request->session()->get('logininfo');
                //    dd($sess);
                    return redirect($checker['redirect'])->with('success',$checker['sms']);

                }
                else{

                    $this->destroy($id);
                }
            }
        }
        elseif($checker['status'] === false){
            return redirect($checker['redirect'])->with('error',$checker['sms']);
        }
    }


    public function validity($data,$file){
        unset($data['submit']);
        $validity =  array();
        $fext = array('png','jpg','gif','jpeg');

        if(in_array('',$data) || in_array(NULL,$data) || empty($data) || $data['password'] != $data['retype']){

          $validity['status'] = false;
            if(!in_array($file,$fext)){
                $validity['sms'] = "All Fields are required & Invalid File format";
            }
            else{
                $validity['sms'] = "All Fields are required ";
            }
          $validity['redirect'] = '/#toregister';
        }
        else{
            $validity['status'] = true;
            $validity['sms'] =  'Account created successfully! Login Now';
            $validity['data'] = $data;
            $validity['redirect'] = '/';
        }


       $validity['file']=$file;
       return $validity;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function getPasswordResetForm(){


        return view('pages.forgetPassword');
    }

    public function postResetpassword(Request $request){
       $data = DB::table('users')
           ->where('email',$request['email'])->get();
        foreach($data as $email){
            //echo $email;
        }
       // dd($email->email);
        Session::flash('success','Password has been reset successfully!');
        if(@$email->email == $request['email'] && $request['password'] == $request['conf_password'] ){

            $update = User::postResetpassword($request);
            return redirect('/');
        }
        else{
            return redirect('/');
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
