<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wedget extends Model
{
    protected $fillable = ['wedget_name','body'];
}