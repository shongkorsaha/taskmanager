<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Department;
use App\Category;
use Session;
class CategoriesController extends Controller
{
    public $user;
    function __construct(Request $request)
    {
        if(!$request->session()->has('logininfo')){
            return redirect('/')->with('error','You have to Login first');
        }

        $data =  $request->session()->get('logininfo');
        $this->user = $data[0];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allDepartments = Department::getDepartmentName();
        //dd($allCategory);
        return view('category.catadd',['user'=>$this->user, 'Dept'=>$allDepartments]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'catName'=>'required',
            'catDept'=>'required|max:255',
            'catDetails'=>'required|max:255',

        ]);
        $data           = $request->all();
        $insertCategory = Category::storeCategory($data);
        Session::flash('success','Category has been submitted successfully.');
        return redirect('catform');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $cat = Category::showCategory();
        //dd($cat);
        return view('category.showcat',['user'=>$this->user,'category'=>$cat]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cat = Category::showCategoryById($id);
        $dep = User::showDept();
//        dd($cat);
        return view('category.updatecatform',['user'=>$this->user, 'category'=>$cat[0],'dept'=>$dep]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $data = $request->all();
//        dd($data);
       $categoryUpdate = Category::updateCategory($data);
        return redirect('allcat')->with('success','updated successfully.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $request->all();
//        dd($request->id);
        $deleteCategory = Category::destroyCategory($request);
        return redirect('allcat')->with('success','Delete successfully.');
    }
}
