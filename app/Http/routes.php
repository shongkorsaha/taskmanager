<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'welcomeController@index');
    Route::post('/login', 'welcomeController@login');
    Route::get('/forgetpassword','welcomeController@getPasswordResetForm');
    Route::post('/resetpassword','welcomeController@postResetpassword');
    Route::get('/logout', 'welcomeController@logout');
    Route::post('/register', 'welcomeController@register');
    Route::get('/dashboard', 'DashboardController@index');
    Route::get('/dashboard/element', 'DashboardController@DashboradElement');
    Route::get('/reqform', 'TaskController@index');
    Route::post('/reqformsubmit', 'TaskController@create');
    Route::get('/reqstatus', 'TaskController@index');
    Route::get('/allreq', 'TaskController@show');
    Route::get('/reqformupdate/{id}', 'TaskController@update');
    Route::post('/reqformupdatesave', 'TaskController@updatesave');


    Route::get('/catform', 'CategoriesController@index');
    Route::post('/catformsubmit', 'CategoriesController@store');
    Route::get('/allcat', 'CategoriesController@show');
    Route::get('/updatecatform/{categoriesid}', 'CategoriesController@edit');
    Route::post('/catupdate/{id}', 'CategoriesController@update');
    Route::get('/deletecat/{id}', 'CategoriesController@destroy');


    Route::get('/newdept', 'DepartmentsController@index');
    Route::post('/adddept', 'DepartmentsController@store');
    Route::get('/alldept', 'DepartmentsController@show');
    Route::get('/updatedeptform/{deptid}', 'DepartmentsController@editform');
    Route::post('/updatedept/{id}', 'DepartmentsController@update');
    Route::get('/deletedept/{id}', 'DepartmentsController@destroy');

    Route::get('/newdeg', 'DesignationsController@index');
    Route::post('/adddeg', 'DesignationsController@store');
    Route::get('/alldeg', 'DesignationsController@show');
    Route::get('/updatedegform/{id}', 'DesignationsController@editform');
    Route::post('/updatedeg/{id}', 'DesignationsController@update');
    Route::get('/deletedeg/{id}', 'DesignationsController@destroy');

    Route::get('/allnotification', 'Notifications@index');
    Route::post('/newnotification', 'Notifications@create');
    Route::get('/updatenotification', 'Notifications@edit');

    Route::get('/allprioritie', 'priorities@index');
    Route::post('/newprioritie', 'priorities@create');
    Route::get('/updatprioritie', 'priorities@edit');

    Route::get('/alluser', 'UserController@index');
    Route::get('/newuserform', 'UserController@showUserCreateForm');
    Route::post('/createuser', 'UserController@store');
    Route::get('/updateuserform/{id}', 'UserController@showUserUpdateForm');
    Route::post('/updateuser/{id}', 'UserController@update');

    Route::get('/dashboard/excel','ExcelController@getExport');
    Route::post('/dashboard/excelIndividual','ExcelController@getExportIndividual');




//});

// Begin Wedget Routes

Route::any('api/wedget', 'ApiController@wedgetData');
Route::any('api/wedget-vue', 'ApiController@wedgetVueData');

Route::resource('wedget', 'WedgetController');

// End Wedget Routes
// Begin Wedget Chart Route

Route::get('api/wedget-chart', 'ApiController@wedgetChartData');

// End Wedget Chart Route