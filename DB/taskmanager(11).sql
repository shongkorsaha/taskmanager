-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 26, 2016 at 11:40 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `taskmanager`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `categorie_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `categorie_des` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dept_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `categorie_name`, `categorie_des`, `dept_id`, `created_at`, `updated_at`) VALUES
(1, 'IT Asset Transfer', 'All IT asset related transfer', 1, NULL, NULL),
(2, 'Email Address Creation', 'Email address creation for new joiner. ', 2, NULL, NULL),
(3, 'Prime ID', 'Prime user name and Password creation.', 4, NULL, NULL),
(4, 'USB Access', 'USB Access', 2, NULL, NULL),
(5, 'OS', 'Operating System Issue. PC hang, Slow , Loading, ', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `department_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `department_des` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `department_name`, `department_des`, `created_at`, `updated_at`) VALUES
(1, 'Maintenance and Support', 'maintenance and support', NULL, NULL),
(2, 'Network Infrastructure', 'Network Infrastructure', NULL, NULL),
(3, 'Network Security', 'Network Security', NULL, NULL),
(4, 'DBA', 'Database Administrator', NULL, NULL),
(5, 'Call Center', 'Call Center 24X7 open.', NULL, NULL),
(6, 'CRM', 'Credit Risk Management. ', NULL, NULL),
(7, 'ICC', 'ICC', NULL, NULL),
(9, 'Asset Operations', 'Asset Operations', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `id` int(10) UNSIGNED NOT NULL,
  `designation_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `designation_des` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`id`, `designation_name`, `designation_des`, `created_at`, `updated_at`) VALUES
(1, 'Assistant Officer', 'Assistant Officer', NULL, NULL),
(2, 'Officer', 'Officer', NULL, NULL),
(3, 'Sr.Officer', 'Senior Officer ', NULL, NULL),
(4, 'Principal Officer ', 'Principal Officer ', NULL, NULL),
(5, 'Assistant Manager', 'Assistant Manager', NULL, NULL),
(6, 'Manager', 'Manager', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_06_25_143325_create_tasks_table', 1),
('2016_06_25_143453_create_notifications_table', 1),
('2016_06_25_143523_create_statuses_table', 1),
('2016_06_25_143544_create_categories_table', 1),
('2016_06_25_143956_create_priorities_table', 1),
('2016_06_25_144029_create_replays_table', 1),
('2016_06_25_144105_create_usertypes_table', 1),
('2016_06_25_144126_create_designations_table', 1),
('2016_06_25_144243_create_departments_table', 1),
('2016_06_25_144750_create_permissions_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `notification_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notification_des` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permission_value` int(11) NOT NULL,
  `permission_des` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `priorities`
--

CREATE TABLE `priorities` (
  `id` int(10) UNSIGNED NOT NULL,
  `prioritie_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prioritie_des` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `priorities`
--

INSERT INTO `priorities` (`id`, `prioritie_name`, `prioritie_des`, `created_at`, `updated_at`) VALUES
(1, 'Low', 'In general', NULL, NULL),
(2, 'Medium', 'Medium', NULL, NULL),
(3, 'High', 'Emergency ', NULL, NULL),
(4, 'Extra High', 'Extra High', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `replays`
--

CREATE TABLE `replays` (
  `id` int(10) UNSIGNED NOT NULL,
  `replay_des` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `replay_date` datetime NOT NULL,
  `task_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `statuses`
--

CREATE TABLE `statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `statuse_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `statuse_des` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `statuses`
--

INSERT INTO `statuses` (`id`, `statuse_name`, `statuse_des`, `created_at`, `updated_at`) VALUES
(0, 'Pending', 'pending', NULL, NULL),
(1, 'Authorized', 'authorized by dept head', NULL, NULL),
(2, 'Approved', 'approved by head of IT or HR or DMD or MD', NULL, NULL),
(3, 'Deny', 'deny', NULL, NULL),
(4, 'Done', 'Done', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `task_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `task_des` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `task_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `task_closed_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `assigned_id` int(11) NOT NULL,
  `user_dept` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `priority_id` int(11) NOT NULL,
  `task_updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `task_name`, `task_des`, `photo`, `task_created_at`, `task_closed_at`, `status_id`, `user_id`, `assigned_id`, `user_dept`, `category_id`, `priority_id`, `task_updated_at`, `updated_by`) VALUES
(10, 'Laptop ', 'up', 'none', '2016-08-18 20:54:46', '2016-08-20 13:37:13', 4, 6, 5, 0, 1, 1, '0000-00-00 00:00:00', 5),
(11, 'User name password', 'prime e user name lagbe', 'none', '2016-08-05 21:04:34', '0000-00-00 00:00:00', 4, 5, 5, 0, 3, 2, '0000-00-00 00:00:00', 5),
(12, 'Laptop Transfer', 'My laptop needs to transfer to IT.', 'none', '2016-07-29 19:32:43', '0000-00-00 00:00:00', 2, 7, 0, 6, 1, 1, '0000-00-00 00:00:00', 5),
(13, 'New email', 'I need new email', 'none', '2016-07-14 19:42:45', '0000-00-00 00:00:00', 3, 6, 0, 0, 2, 1, '0000-00-00 00:00:00', 0),
(14, 'Prime needed.', 'update Prime needed.', 'none', '2016-07-29 19:35:02', '0000-00-00 00:00:00', 2, 12, 0, 6, 1, 2, '0000-00-00 00:00:00', 6),
(15, 'Printer', '1 Printer to IT', 'none', '2016-07-14 19:48:22', '0000-00-00 00:00:00', 2, 12, 0, 6, 1, 1, '0000-00-00 00:00:00', 12),
(16, 'email', 'slflsjflsj', 'none', '2016-07-17 19:20:26', '0000-00-00 00:00:00', 3, 14, 0, 0, 1, 2, '0000-00-00 00:00:00', 5),
(17, 'Prime needed.', 'prime', 'none', '2016-07-14 18:44:04', '0000-00-00 00:00:00', 2, 14, 0, 6, 3, 1, '0000-00-00 00:00:00', 0),
(18, 'jsldjlfs', 'ldjlfsjflj', 'jpg', '2016-08-25 19:48:47', '2016-08-25 13:48:32', 4, 6, 5, 4, 2, 1, '0000-00-00 00:00:00', 5),
(19, 'PRIME', 'PRIME', 'none', '2016-07-17 19:28:41', '0000-00-00 00:00:00', 2, 12, 0, 6, 3, 1, '0000-00-00 00:00:00', 6),
(20, 'email', 'create my email', 'none', '2016-07-18 14:26:49', '0000-00-00 00:00:00', 0, 19, 0, 7, 2, 1, '0000-00-00 00:00:00', 0),
(21, 'Create email', 'create my email', 'none', '2016-07-18 14:28:09', '0000-00-00 00:00:00', 0, 18, 0, 7, 2, 1, '0000-00-00 00:00:00', 0),
(22, 'jsdlfjslfj', 'ljflsjflsjf', 'none', '2016-07-20 18:40:57', '0000-00-00 00:00:00', 0, 6, 0, 1, 2, 1, '0000-00-00 00:00:00', 0),
(23, 'email for test', 'testing email', 'none', '2016-08-05 20:04:26', '0000-00-00 00:00:00', 4, 12, 5, 6, 2, 1, '0000-00-00 00:00:00', 5),
(24, 'email', 'lsflsjf', 'none', '2016-07-29 19:35:10', '0000-00-00 00:00:00', 2, 12, 0, 6, 2, 1, '0000-00-00 00:00:00', 6),
(25, 'Prime', 'Prime access sflsjfljs', 'none', '2016-07-29 19:42:53', '0000-00-00 00:00:00', 2, 22, 0, 9, 3, 1, '0000-00-00 00:00:00', 6),
(26, 'Laptop Transfer', 'slfslfj', 'none', '2016-07-29 20:00:37', '0000-00-00 00:00:00', 4, 22, 5, 9, 1, 1, '0000-00-00 00:00:00', 5),
(27, 'Prime', 'lsjflsjflj', 'none', '2016-08-05 21:04:43', '0000-00-00 00:00:00', 4, 20, 5, 9, 3, 1, '0000-00-00 00:00:00', 5),
(28, 'email', 'lsflsjf', 'none', '2016-07-29 18:22:15', '0000-00-00 00:00:00', 0, 12, 0, 6, 2, 1, '0000-00-00 00:00:00', 0),
(29, '.xcmv.xm', 'djgljdljdg', 'none', '2016-07-29 19:45:49', '0000-00-00 00:00:00', 1, 22, 0, 9, 1, 1, '0000-00-00 00:00:00', 21),
(30, 'asdfa', 'adsfasdfsadfs', 'none', '2016-08-05 20:04:32', '0000-00-00 00:00:00', 4, 6, 5, 1, 2, 1, '0000-00-00 00:00:00', 5),
(31, 'jflsjlj', 'sjflsjflsjfs', 'none', '2016-07-30 14:59:27', '0000-00-00 00:00:00', 0, 12, 0, 6, 1, 1, '0000-00-00 00:00:00', 0),
(32, '', '', 'none', '2016-07-30 14:59:38', '0000-00-00 00:00:00', 0, 12, 0, 6, 0, 1, '0000-00-00 00:00:00', 0),
(33, '', '', 'none', '2016-07-30 14:59:51', '0000-00-00 00:00:00', 0, 12, 0, 6, 0, 1, '0000-00-00 00:00:00', 0),
(34, 'sjdflsfj', 'dljfslfj', 'none', '2016-07-30 15:31:14', '0000-00-00 00:00:00', 1, 22, 5, 9, 2, 1, '0000-00-00 00:00:00', 21),
(35, 'Laptop Transfer', 'My laptop needs transfer to IT.', 'none', '2016-08-05 17:03:35', '0000-00-00 00:00:00', 1, 22, 5, 9, 1, 1, '0000-00-00 00:00:00', 21),
(36, '', '', 'none', '2016-08-02 16:37:10', '0000-00-00 00:00:00', 0, 6, 0, 1, 0, 1, '0000-00-00 00:00:00', 0),
(37, '', '', 'none', '2016-08-02 16:38:03', '0000-00-00 00:00:00', 0, 6, 0, 1, 0, 1, '0000-00-00 00:00:00', 0),
(38, 'GJHGJG', 'gjgjg', 'none', '2016-08-06 05:48:34', '0000-00-00 00:00:00', 1, 22, 5, 9, 1, 1, '0000-00-00 00:00:00', 21),
(39, 'Email', 'create email for me.', 'none', '2016-08-05 20:25:54', '0000-00-00 00:00:00', 4, 20, 5, 9, 2, 1, '0000-00-00 00:00:00', 5),
(40, 'Laptop Transfer', 'test', 'none', '2016-08-26 18:53:16', '2016-08-26 12:51:09', 2, 22, 5, 9, 1, 1, '0000-00-00 00:00:00', 6),
(41, 'fslfj', 'dljflsjl', 'none', '2016-08-04 15:05:24', '0000-00-00 00:00:00', 0, 6, 0, 1, 1, 1, '0000-00-00 00:00:00', 0),
(42, 'skfjslfjaslfj', 'skjflasfjlsjf', 'none', '2016-08-04 15:06:19', '0000-00-00 00:00:00', 0, 6, 0, 1, 2, 1, '0000-00-00 00:00:00', 0),
(43, 'my email', 'email', 'none', '2016-08-05 15:25:53', '0000-00-00 00:00:00', 2, 5, 5, 1, 2, 1, '0000-00-00 00:00:00', 5),
(44, 'my phone', 'phone', 'none', '2016-08-04 18:23:02', '0000-00-00 00:00:00', 0, 5, 0, 1, 1, 1, '0000-00-00 00:00:00', 0),
(45, 'test email', 'test', 'none', '2016-08-04 19:25:31', '0000-00-00 00:00:00', 0, 6, 0, 1, 2, 1, '0000-00-00 00:00:00', 0),
(46, 'hgjhfjf', 'gjfjfj', 'none', '2016-08-05 10:36:57', '0000-00-00 00:00:00', 0, 20, 0, 9, 1, 1, '0000-00-00 00:00:00', 0),
(47, 'Laptop Transfer ', 'Laptop transfer to dept. ', 'none', '2016-08-05 10:51:12', '0000-00-00 00:00:00', 0, 20, 0, 9, 1, 1, '0000-00-00 00:00:00', 0),
(48, 'skjlfjlfj', 'kjflsjfldjflj', 'none', '2016-08-05 17:05:56', '0000-00-00 00:00:00', 1, 21, 5, 9, 1, 1, '0000-00-00 00:00:00', 21),
(49, 'jlfjlfj', 'by sharmin', 'none', '2016-08-05 13:55:38', '0000-00-00 00:00:00', 4, 21, 5, 9, 1, 1, '0000-00-00 00:00:00', 5),
(50, 'email', 'by Monia Tanha', 'none', '2016-08-05 13:56:02', '0000-00-00 00:00:00', 0, 5, 0, 1, 2, 1, '0000-00-00 00:00:00', 0),
(51, 'ok', 'ok', 'none', '2016-08-05 15:19:19', '0000-00-00 00:00:00', 0, 5, 0, 1, 2, 1, '0000-00-00 00:00:00', 0),
(52, 'email', 'by sanker.saha', 'none', '2016-08-05 16:26:15', '0000-00-00 00:00:00', 0, 6, 0, 1, 2, 1, '0000-00-00 00:00:00', 0),
(53, 'sfsf', 'by sharmin', 'none', '2016-08-05 17:03:11', '0000-00-00 00:00:00', 1, 21, 5, 9, 1, 1, '0000-00-00 00:00:00', 21),
(54, 'Prime user', 'Prime user name and password creation. ', 'none', '2016-08-05 21:01:14', '0000-00-00 00:00:00', 0, 20, 0, 9, 2, 1, '0000-00-00 00:00:00', 0),
(55, 'bcbcbc', 'hfhfhc', 'none', '2016-08-25 19:49:16', '2016-08-25 13:49:03', 4, 20, 5, 9, 3, 1, '0000-00-00 00:00:00', 5),
(56, 'USB Access needed', 'USB Access needed for scanner. ', 'none', '2016-08-18 18:55:26', '0000-00-00 00:00:00', 4, 20, 5, 9, 4, 1, '0000-00-00 00:00:00', 5),
(57, 'Loading ', 'lsflsflsjf', 'none', '2016-08-24 18:18:50', '2016-08-24 12:18:44', 4, 6, 5, 1, 5, 1, '0000-00-00 00:00:00', 5),
(58, 'OS', 'sljflsfjslj', 'none', '2016-08-19 15:25:12', '0000-00-00 00:00:00', 0, 6, 0, 1, 5, 1, '0000-00-00 00:00:00', 0),
(59, 'hang', 'ljslfjal', 'none', '2016-08-25 17:11:12', '0000-00-00 00:00:00', 0, 12, 0, 6, 5, 1, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `emp_id` int(11) NOT NULL,
  `full_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_type` int(11) NOT NULL,
  `user_dept` int(11) NOT NULL,
  `user_deg` int(11) NOT NULL,
  `office_cell` int(11) NOT NULL,
  `home_cell` int(11) NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `emp_id`, `full_name`, `username`, `password`, `user_type`, `user_dept`, `user_deg`, `office_cell`, `home_cell`, `address`, `photo`, `remember_token`, `created_at`, `updated_at`) VALUES
(5, 'monia.tanha@diu.com', 1405629, 'Monia Tanha', 'monia.tanha', 'c4ca4238a0b923820dcc509a6f75849b', 2, 1, 2, 1736102846, 1736102846, 'ajdlfj', 'jpg', NULL, NULL, NULL),
(6, 'sanker.saha@diu.com', 1405621, 'Sanker Saha', 'sanker.saha', 'c4ca4238a0b923820dcc509a6f75849b', 3, 1, 2, 1736102846, 1736102846, 'Mirpur 14', 'jpg', NULL, NULL, NULL),
(7, 'ashikur.rahman@diu.com', 1405627, 'Ashikur Rahman', 'ashikur.rahman', 'c4ca4238a0b923820dcc509a6f75849b', 2, 4, 2, 1719118833, 1719118833, 'Tangail', 'jpg', NULL, NULL, NULL),
(8, 'mahmudul.tuhin@diu.com', 1405721, 'Mahmudul Hasan Tuhin', 'mahmudul.tuhin', 'c4ca4238a0b923820dcc509a6f75849b', 3, 2, 5, 1671067476, 1671067476, 'DIU', 'jpg', NULL, NULL, NULL),
(12, 'test@gmail.com', 1505627, 'test', 'test', 'c4ca4238a0b923820dcc509a6f75849b', 0, 6, 1, 1736102846, 1736102846, 'ldsjlfj', 'jpg', NULL, NULL, NULL),
(13, 'bishwanath.saha@diu.com', 16061000, 'Bishwanath Saha', 'bishwanath.saha', 'c4ca4238a0b923820dcc509a6f75849b', 1, 6, 5, 1736102846, 1736102846, 'ST level 3', 'jpg', NULL, NULL, NULL),
(14, 'barsha.saha@diu.com', 16061001, 'Barsha Saha', 'barsha.saha', 'c4ca4238a0b923820dcc509a6f75849b', 0, 6, 1, 1736102846, 1736102846, 'ST 05', 'jpg', NULL, NULL, NULL),
(15, 'paihelichakma13@gmail.com', 1607501, 'paiheli chakma', 'paiheli', 'c4ca4238a0b923820dcc509a6f75849b', 0, 4, 5, 1701890198, 1701890198, 'mirpur,dhaka ', 'jpg', NULL, NULL, NULL),
(16, 'gazi.mozahid@diu.com', 16061010, 'Gazi Mozahid ', 'gazi.mozahid', 'c4ca4238a0b923820dcc509a6f75849b', 1, 6, 5, 1736102846, 1736102846, 'fgdgdgdsg', 'jpg', NULL, NULL, NULL),
(17, 'kamrul.islam@diu.com', 16061011, 'Kamrul Islam', 'kamrul.islam', 'c4ca4238a0b923820dcc509a6f75849b', 0, 7, 5, 1736102846, 1736102846, 'ST 14 ', 'jpg', NULL, NULL, NULL),
(18, 'shofique@diu.com', 16061013, 'Shofique', 'shofique', 'c4ca4238a0b923820dcc509a6f75849b', 0, 7, 4, 1736102846, 1736102846, 'Address ', 'jpg', NULL, NULL, NULL),
(19, 'razib@diu.com', 16061014, 'Razib', 'razib', 'c4ca4238a0b923820dcc509a6f75849b', 0, 7, 3, 1736102846, 1736102846, 'fljflsjgljgl', 'jpg', NULL, NULL, NULL),
(20, 'trishna.palma@diu.com', 1605629, 'Trishna Palma', 'trishna.palma', 'c4ca4238a0b923820dcc509a6f75849b', 0, 9, 2, 1736102846, 1736102846, ' kdfskfsfh', 'jpg', NULL, NULL, NULL),
(21, 'sharmin.sultana@diu.com', 16071029, 'Sharmin Sultana', 'sharmin.sultana', 'c4ca4238a0b923820dcc509a6f75849b', 1, 9, 5, 1736102846, 1736102846, 'jflsjflsjfsljf', 'jpg', NULL, NULL, NULL),
(22, 'trishna.palma1@diu.com', 16071035, 'Trishna Palma 1', 'trishna.palma1', 'c4ca4238a0b923820dcc509a6f75849b', 0, 9, 1, 1736102846, 1736102846, ' hsfhsfhskfh', 'jpg', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `usertypes`
--

CREATE TABLE `usertypes` (
  `access_id` int(10) UNSIGNED NOT NULL,
  `access_level` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `usertypes`
--

INSERT INTO `usertypes` (`access_id`, `access_level`, `created_at`, `updated_at`) VALUES
(0, 'Normal', NULL, NULL),
(1, 'Moderator', NULL, NULL),
(2, 'Admin', NULL, NULL),
(3, 'Supper Admin', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `priorities`
--
ALTER TABLE `priorities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `replays`
--
ALTER TABLE `replays`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tasks_status_id_index` (`status_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_emp_id_unique` (`emp_id`);

--
-- Indexes for table `usertypes`
--
ALTER TABLE `usertypes`
  ADD PRIMARY KEY (`access_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `priorities`
--
ALTER TABLE `priorities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `replays`
--
ALTER TABLE `replays`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `statuses`
--
ALTER TABLE `statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `usertypes`
--
ALTER TABLE `usertypes`
  MODIFY `access_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
