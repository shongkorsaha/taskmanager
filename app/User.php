<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    private $usertable = 'users';

    public static function CreateAccount($data,$ext){
     return   DB::table('users')->insertGetId(
          [
              'emp_id'      => $data['empid'],
              'email'       => $data['email'],
              'full_name'   => $data['fname'],
              'username'    => $data['username'],
              'password'    => md5($data['password']),
              'user_dept'   => $data['dept'],
              'user_deg'    => $data['deg'],
              'user_type'   => $data['usertype'],
              'office_cell' => $data['fphone'],
              'home_cell'   => $data['hphone'],
              'address'     => $data['address'],
              'photo'       => $ext
          ]

        );
    }

    public static function showUserList($id,$accessLavle){

        if($accessLavle==2 || $accessLavle==3){
            return User::showUserListAsAdmin();

        }elseif($accessLavle == 0 || $accessLavle ==1 ){
            return User::showUserListAsNormal();
        }

    }
    public static function showUserListAsAdmin(){
         return DB::table('users')
            ->join('designations', 'designations.id', '=', 'users.user_deg')
            ->join('departments','departments.id','=','users.user_dept')
            ->join('usertypes','usertypes.access_id','=','users.user_type')
            ->select('users.id','users.emp_id','users.full_name','users.email','users.office_cell',
                'designations.designation_name','departments.department_name','usertypes.access_level')->paginate(10);
    }

    public static function showUserListAsNormal(){
        return DB::table('users')
            ->join('designations', 'designations.id', '=', 'users.user_deg')
            ->join('departments','departments.id','=','users.user_dept')
            ->select('users.id','users.emp_id','users.full_name','users.email','users.office_cell',
                'designations.designation_name','departments.department_name')->paginate(10);
    }

    public static function showUserType(){
        return DB::table('usertypes')->get();
    }

    public static function showUserInUpdateForm($id){
       return DB::table('users')
           ->where('users.id',$id)->get();
    }

    public static function showDesignations(){
      return  DB::table('designations')->get();
    }

    public static function showDept(){
        return DB::table('departments')->get();
    }

    public static function updateUserAccess($data){
        return
            DB::table('users')
                ->where('id', $data['id'])
                ->update([
                    'password'    => md5($data['password']),
                    'user_dept' => $data['dept'],
                    'user_deg'  => $data['deg'],
                    'user_type' => $data['usertype']
                ]);
    }

    public static function postResetpassword($request){
        return DB::table('users')
            ->where('email', $request['email'])
            ->update([
                'password'    => md5($request['password'])
            ]);
    }
//for indevidul report
    public static function UserListAsAdmin(){
        return DB::table('users')
            ->join('designations', 'designations.id', '=', 'users.user_deg')
            ->join('departments','departments.id','=','users.user_dept')
            ->join('usertypes','usertypes.access_id','=','users.user_type')
            ->select('users.id','users.emp_id','users.full_name','users.email','users.office_cell',
                'designations.designation_name','departments.department_name','usertypes.access_level')
            ->whereIn('usertypes.access_id',[2,3])
            ->get();
    }


}
