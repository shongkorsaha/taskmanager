@include('partials.header')


<script type="text/javascript">

    // Load Charts and the corechart package.
    google.charts.load('current', {'packages':['corechart']});

    // Draw the pie chart for Sarah's pizza when Charts is loaded.
    google.charts.setOnLoadCallback(drawSarahChart);

    // Draw the pie chart for the Anthony's pizza when Charts is loaded.
    google.charts.setOnLoadCallback(drawAnthonyChart);

    // Callback that draws the pie chart for Sarah's pizza.
    function drawSarahChart() {

        // Create the data table for Sarah's pizza.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Data Bar');
        data.addRows([
            ['Pending', {{ $tasks['Pending']}}],
            ['Authorized', {{ $tasks['Authorized']}}],
            ['Approved', {{ $tasks['Approved']}}],
            ['Deny', {{ $tasks['Deny']}}],
            ['Done', {{ $tasks['Done']}}]
        ]);

        // Set options for Sarah's pie chart.
        var options = {title:'Requisitions Status Chart',
            };

        // Instantiate and draw the chart for Sarah's pizza.
        var chart = new google.visualization.BarChart(document.getElementById('Sarah_chart_div'));
        chart.draw(data, options);
    }

    // Callback that draws the pie chart for Anthony's pizza.
    function drawAnthonyChart() {

        // Create the data table for Anthony's pizza.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
            <?php
            foreach($paiChartData as $key=>$value){
        echo '['.'"'.$value->categorie_name.'",'.$value->total.'],';

            }
            ?>
           ["DEFAULT", 0]
//            ['Onions', 2],
//            ['Olives', 2],
//            ['Zucchini', 0],
//            ['Pepperoni', 3]
        ]);

        // Set options for Anthony's pie chart.
        var options = {title:'Task Category wais Ratio',
           };

        // Instantiate and draw the chart for Anthony's pizza.
        var chart = new google.visualization.PieChart(document.getElementById('Anthony_chart_div'));
        chart.draw(data, options);
    }
</script>

{{--{{print_r($paiChartData)}}--}}
{{----}}





{{--<meta http-equiv="refresh" content="20">--}}
        <!-- page content -->
<div class="right_col" role="main">

    <!-- top tiles -->
    <div class="row tile_count">
        <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">

            <div class="right orange-bg">
                <span class="count_top"> Total Pending </span>
                <div class="icon-wrapper"><i aria-hidden="true" class="fa fa-eye-slash"></i></div>
                <div id="pending" class="count"> {{ $tasks['Pending']}}</div>

            </div>
        </div>

        <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">

            <div class="right blue-bg">
                <span class="count_top">Total Authorized</span>
                <div class="icon-wrapper"><i aria-hidden="true" class="fa fa-users"></i></div>
                <div  id="Authorized"  class="count green">{{ $tasks['Authorized']}}</div>

            </div>
        </div>
        <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">

            <div class="right green-bg">
                <span class="count_top">Total Approved</span>
                <div class="icon-wrapper"><i aria-hidden="true" class="fa fa-thumbs-o-up"></i></div>
                <div id="Approved" class="count">{{ $tasks['Approved']}}</div>

            </div>
        </div>
        <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
            <div class="right red-bg">
                <span class="count_top"> Total Deny</span>
                <div class="icon-wrapper"><i aria-hidden="true" class="fa fa-exclamation-triangle"></i></div>
                <div id="Deny" class="count">{{ $tasks['Deny']}}</div>

            </div>

        </div>
        <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">

            <div class="right sky-blue-bg">
                <span class="count_top">Total Done</span>
                <div class="icon-wrapper"><i aria-hidden="true" class="fa fa-check-square-o"></i></div>
                <div id="Done" class="count">{{ $tasks['Done']}}</div>

            </div>
        </div>

    </div>

    <br />
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">

            <div id="Sarah_chart_div" style="border: 1px solid #ccc; width: 100%; height: 300px;"></div>

        </div>

    </div>
    <br />
    <div class="row">

        <div class="col-md-6 col-sm-4 col-xs-12">

            <div id="Anthony_chart_div" style="border: 1px solid #ccc ;width: 100%; height: 300px;"></div>
        </div>


        <div class="col-md-6 col-sm-4 col-xs-12">
            <div class="x_panel tile reports-panel">
                <div class="x_title">
                    <h2>Get Reports</h2>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="dashboard-widget-content">

                        <div class="report-wrapper">
                            <h4>To get all reports just click here.</h4>
                            <ul class="quick-list">
                                <li>
                                    <a href="{{ URL::to('/dashboard/excel') }}" class="btn btn-success" style="float: right;">All Report  &nbsp;<span class="glyphicon glyphicon-download-alt"></span> </a>
                                </li>
                            </ul>
                        </div>

                        <form  action="{{URL::to('/dashboard/excelIndividual')}}"  method="POST" class="form-inline report-form">
                            <h4>To get specific reports just select start date and end date.</h4>
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <!-- <label for="exampleInputName2">Start Date</label> -->
                                    <input type="text" name="startdate" class="form-control datepicker " id="datepicker" placeholder="Start Date">
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <!-- <label for="exampleInputEmail2">End Date</label> -->
                                    <input type="text" name="enddate" class="form-control datepicker"  id="datepicker1" placeholder="End Date">
                                </div>
                            </div>
                            @if($user->user_type > 2 )
                                <div class="col-sm-12 col-md-6">
                                    <div class="form-group">
                                        <select class="form-control"  name="userTypeId">
                                            <option>-- Users --</option>
                                            @foreach($individual as $adminUser)
                                                <option value="{{$adminUser->id}}">{{$adminUser->full_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endif
                            {{ csrf_field() }}
                            <div class="col-xs-12">
                                <div class="col-xs-12">
                                    <ul class="quick-list">
                                        <li>
                                            <button type="submit" name="submit" class="btn btn-success" >Individual Report &nbsp;<span class="glyphicon glyphicon-download-alt"></span> </button>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>


</div>

</div>
</div>


<script>
    $(function() {
        $( "#datepicker" ).datepicker({
            dateFormat: 'yy/mm/dd'
        });
    });
</script>
<script>
    $(function() {
        $( "#datepicker1" ).datepicker({
            dateFormat: 'yy/mm/dd'
        });
    });
</script>

@include('partials.footer')


