<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\URL;

class Task extends Model
{
    public static function store($data){
        return   DB::table('tasks')->insertGetId(
            [
                'user_id'       => $data['id'],
                'user_dept'     => $data['user_dept'],
                'category_id'   => $data['tasktype'],
                'task_name'     => $data['taskName'],
                'task_des'      => $data['detailsInput'],
                'priority_id'   => 1,
                'status_id'     => 0,
                'photo'         => $data['ext']

            ]

        );
    }

    public static function showreq($user){

        if($user->user_type == 2){
            return Task::LevelAdmin($user->id);
        }
        elseif($user->user_type==3){
            return Task::LevelSuperAdmin($user->id);
        }
        elseif($user->user_type == 0){
            return Task::LevelNormal($user->id);
        }

        elseif($user->user_type == 1){
            return Task::LevelModerator($user->user_dept);
        }


    }

    Public static function LevelSuperAdmin ($id){
        $data = DB::table('tasks')
            ->join('categories','categories.id' ,'=', 'tasks.category_id')
            ->join('users', 'users.id', '=', 'tasks.user_id')
            ->join('priorities','priorities.id','=','tasks.priority_id')
            ->join('statuses','statuses.id','=','tasks.status_id')
            ->select('tasks.id as taskid',
                'categories.categorie_name as categorie_name',
                'tasks.task_des as task_des',
                'users.email as email',
                'tasks.user_id as user_id',
                'priorities.prioritie_name as task_priority',
                'statuses.statuse_name as status',
                'statuses.id as statusId',
                'tasks.updated_by as updated_by' )
            ->where('tasks.status_id',1)
            ->orWhere('user_id', $id)
            ->orderBy('taskid', 'asc')
            ->paginate(10);

        return $data;
    }

    Public static function LevelAdmin ($id){
        $data = DB::table('tasks')
            ->join('categories','categories.id' ,'=', 'tasks.category_id')
            ->join('users', 'users.id', '=', 'tasks.user_id')

            ->join('priorities','priorities.id','=','tasks.priority_id')
            ->join('statuses','statuses.id','=','tasks.status_id')
            ->select('tasks.id as taskid',
                'categories.categorie_name as categorie_name',
                'tasks.task_des as task_des',
                'users.email as email',
                'priorities.prioritie_name as task_priority',
                'statuses.statuse_name as status',
                'statuses.id as statusId',
                'tasks.updated_by as updated_by')

            ->where(['tasks.user_id'=>$id])
            ->orWhere(['tasks.assigned_id'=>$id])
            ->whereIn('tasks.status_id',[0,2])
            ->orderBy('taskid', 'asc')
            ->paginate(10);

        return $data;
    }

    public static function LevelNormal($id){
        $data = DB::table('tasks')
            ->join('categories','categories.id' ,'=', 'tasks.category_id')
            ->join('users', 'users.id', '=', 'tasks.user_id')
            ->join('priorities','priorities.id','=','tasks.priority_id')
            ->join('statuses','statuses.id','=','tasks.status_id')
            ->select('tasks.id as taskid',
                'categories.categorie_name as categorie_name',
                'tasks.task_des as task_des',
                'users.email as email',
                'priorities.prioritie_name as task_priority',
                'statuses.statuse_name as status',
                'statuses.id as statusId',
                'tasks.updated_by as updated_by' )
            ->where('tasks.user_id',$id)
            ->orderBy('taskid', 'desc')
            ->paginate(10);

        return $data;
    }

    public static function LevelModerator($user_dept){
        $data = DB::table('tasks')
            ->join('categories','categories.id' ,'=', 'tasks.category_id')
            ->join('users', 'users.id', '=', 'tasks.user_id')
            ->join('priorities','priorities.id','=','tasks.priority_id')
            ->join('statuses','statuses.id','=','tasks.status_id')
            ->select('tasks.id as taskid',
                'categories.categorie_name as categorie_name',
                'tasks.task_des as task_des',
                'users.email as email',
                'priorities.prioritie_name as task_priority',
                'statuses.statuse_name as status',
                'statuses.id as statusId',
                'tasks.updated_by as updated_by' )
            ->where('tasks.user_dept',$user_dept)
            ->whereIn('tasks.status_id',[0,3])
            ->orderBy('taskid', 'asc')
            ->paginate(10);

        return $data;
    }
    public static function  get_updated_by($id){
         $data =  DB::table('users')->select('full_name')->where('id',$id)->get();
        return $data[0]->full_name;
    }


   public static function findTaskRowNumber($id){
      return $data = DB::table('tasks')->where('id',$id)->get();

   }


    public static function updateTask($data){

       return DB::table('tasks')->where('id',$data['taskRowId'])
            ->update(
                ['task_name'        =>$data['taskName'],
                 'task_des'         =>$data['taskdetail'],
                 'category_id'      =>$data['tasktype'],
                 'priority_id'      =>$data['taskcategory'],
                'assigned_id'       =>$data['assignedTo'],
                 'updated_by'       =>$data['userid'],
                'status_id'         =>$data['statusId'],
                'task_closed_at'   =>$data['closeDate']]);
    }

    public static function userlist(){

       return DB::table('users')
        ->join('departments','departments.id','=','users.user_dept')
        ->select('users.id as userid', 'users.email as useremail', 'users.user_type','departments.department_name as userdept','departments.id as deptid')
        ->where('users.user_dept',1)->get();
    }

    public static function allTask(){
        $data = array();
        $data['Pending']    = DB::table('tasks')->where('status_id',0)->count();
        $data['Authorized'] = DB::table('tasks')->where('status_id',1)->count();
        $data['Approved']   = DB::table('tasks')->where('status_id',2)->count();
        $data['Deny']       = DB::table('tasks')->where('status_id',3)->count();
        $data['Done']       = DB::table('tasks')->where('status_id',4)->count();
        $list       = DB::table('tasks')->join('users', 'users.id', '=', 'tasks.user_id')
                                        ->join('categories', 'categories.id', '=', 'tasks.category_id')
                                                ->select('tasks.id as taskid',
                                                    'categories.categorie_name as categorie_name',
                                                    'tasks.task_des as task_des',
                                                    'tasks.status_id as status_id',
                                                    'users.full_name as user_full_name',
                                                    'users.id as userid',
                                                    'users.photo as photo',
                                                    'tasks.task_created_at')
                                                ->where('tasks.status_id',0)->take(4)->orderBy('tasks.id','desc')->get();

        $li =  '';
            foreach($list as $l):
          $li .= '<li>
                                    <a href="'.URL::to("/reqformupdate/".$l->taskid).'">
                                            <span class="image">
                                                <img src="'.URL::to("/photos/".$l->userid.'.'.$l->photo) .' " alt="Profile Image" />
                                            </span>
                                            <span>
                                                <span>'.$l->user_full_name.'</span>
                                                <span class="time">'.date("d M, Y" , strtotime($l->task_created_at)).'</span>
                                            </span>
                                            <span class="message">
                                                '.$l->categorie_name.'
                                            </span>
                                            <span class="message">
                                                '.$l->task_des.'
                                            </span>
                                    </a>
                                </li>';
            endforeach;
          $data['list'] = $li;
        return $data;
    }


    public static function paiChartTaskDoneByCat(){

        $data = DB::table('tasks')
            ->join('categories', 'categories.id', '=', 'tasks.category_id')
            ->select(DB::raw('count(*) as total'),'tasks.id as taskid',
                'categories.categorie_name as categorie_name')
            ->where('tasks.status_id',4)
            ->groupBy('tasks.category_id')
            ->get();

       return $data;
    }




    public static function allDoneJobForExel(){
        $total = DB::table('tasks')->where('tasks.status_id',4)->count();
      return DB::table('tasks')
            ->join('categories', 'categories.id', '=', 'tasks.category_id')
            ->select(DB::raw('count(*) as total'),'tasks.id as taskid',
                'categories.categorie_name as categorie_name')
            ->where('tasks.status_id',4)
            ->groupBy('tasks.category_id')
            ->get();

    }

    public static function individualDoneTask($request, $user){

        if($user->user_type > 2){
           return Task::individualDoneTaskSuperAdmin($request);

        }
        else{
            return Task::individualDoneTaskFrom($request, $user);

        }


    }

    public static function individualDoneTaskSuperAdmin($request){
//         return  DB::table('tasks')
//            ->join('categories','categories.id' ,'=', 'tasks.category_id')
//            ->join('users', 'users.id', '=', 'tasks.user_id')
//            ->join('priorities','priorities.id','=','tasks.priority_id')
//            ->join('statuses','statuses.id','=','tasks.status_id')
//            ->select('tasks.id as taskid',
//                'users.full_name as full_name',
//                'categories.categorie_name as categorie_name',
//                'tasks.task_des as task_des',
//                'users.email as email',
//                'statuses.statuse_name as status',
//                'tasks.updated_by as updated_by',
//                'task_created_at as task_created_at',
//                'task_closed_at as task_closed_at')
//            ->where('users.id',$request->userTypeId )->get();
//             ->where(DB::raw("DATE_FORMAT(task_created_at,'%Y-%m-%d') >= '".date('Y-m-d',strtotime($request->startdate))."'"))
//             ->where(DB::raw("DATE_FORMAT(task_closed_at,'%Y-%m-%d') <= '".date('Y-m-d',strtotime($request->enddate))."'"))->get();
//
////        ->whereBetween('votes', [1, 100])->get();

        return $results =   DB::select("SELECT
  tasks.id AS taskid,
  tasks.user_id AS doneby,
  users.full_name AS full_name,
  categories.categorie_name AS categorie_name,
  tasks.task_des AS task_des,
  users.email AS email,
  statuses.statuse_name AS STATUS,
  tasks.updated_by AS updated_by,
  task_created_at AS task_created_at,
  task_closed_at AS task_closed_at
FROM
  tasks
	INNER JOIN categories
    	ON categories.id = tasks.category_id
  	INNER JOIN users
    	ON users.id = tasks.user_id
    INNER JOIN  priorities
        ON priorities.id = tasks.priority_id
  	INNER JOIN  statuses
	    ON statuses.id = tasks.status_id

 WHERE tasks.user_id = $request->userTypeId
 AND  DATE_FORMAT(task_created_at,'%Y/%m/%d') >='$request->startdate'
AND DATE_FORMAT(task_closed_at,'%Y/%m/%d') <='$request->enddate' AND tasks.status_id = 4");


    }


    public static function individualDoneTaskFrom($request, $user){
        return $results = DB::select("SELECT
  tasks.id AS taskid,
  tasks.user_id AS doneby,
  users.full_name AS full_name,
  categories.categorie_name AS categorie_name,
  tasks.task_des AS task_des,
  users.email AS email,
  statuses.statuse_name AS STATUS,
  tasks.updated_by AS updated_by,
  task_created_at AS task_created_at,
  task_closed_at AS task_closed_at
FROM
  tasks
	INNER JOIN categories
    	ON categories.id = tasks.category_id
  	INNER JOIN users
    	ON users.id = tasks.user_id
    INNER JOIN  priorities
        ON priorities.id = tasks.priority_id
  	INNER JOIN  statuses
	    ON statuses.id = tasks.status_id

 WHERE tasks.user_id = $user->id
 AND  DATE_FORMAT(task_created_at,'%Y/%m/%d') >='$request->startdate'
AND DATE_FORMAT(task_closed_at,'%Y/%m/%d') <='$request->enddate' AND tasks.status_id = 4");

    }



}
