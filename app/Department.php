<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Department extends Model
{
    public static function store($data){
        return   DB::table('departments')->insertGetId(
            [
                'department_name'     => $data['deptName'],
                'department_des'      => $data['deptDetails']


            ]

        );

    }
//show department info as pagination
    public static function dbshow(){
       return DB::table('departments')->get();
    }
//show department list
    public static function getDepartmentName(){
        return $data = DB::table('departments')->get();
    }

//show department form for update
    public static function showDeptById($id){
        return DB::table('departments')
            ->where('id',$id)
            ->get();
    }

    public static function updateDept($data){
        return
            DB::table('departments')
                ->where('id', $data['id'])
                ->update([
                    'department_name' => $data['department_name'],
                    'department_des'  => $data['department_des']
                ]);
    }

//delete department by id
    public static function destroyDeptById($request){
        return DB::table('departments')
            ->where('id', $request->id)
            ->delete();
    }
}
