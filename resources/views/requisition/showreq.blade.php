@include('partials.header')

            <!-- page content -->
            <div class="right_col" role="main">
                <div class="">
                    {{--<div class="page-title">--}}
                        {{--<div class="title_left">--}}
                            {{--<h3>--}}
                    {{--Invoice--}}
                    {{--<small>--}}
                        {{--Some examples to get you started--}}
                    {{--</small>--}}
                {{--</h3>--}}
                        {{--</div>--}}

                        {{--<div class="title_right">--}}
                            {{--<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">--}}
                                {{--<div class="input-group">--}}
                                    {{--<input type="text" class="form-control" placeholder="Search for...">--}}
                                    {{--<span class="input-group-btn">--}}
                            {{--<button class="btn btn-default" type="button">Go!</button>--}}
                        {{--</span>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="clearfix"></div>

                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Requisition List </h2>
                                    {{--<ul class="nav navbar-right panel_toolbox">--}}
                                        {{--<li><a href="#"><i class="fa fa-chevron-up"></i></a>--}}
                                        {{--</li>--}}
                                        {{--<li class="dropdown">--}}
                                            {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>--}}
                                            {{--<ul class="dropdown-menu" role="menu">--}}
                                                {{--<li><a href="#">Settings 1</a>--}}
                                                {{--</li>--}}
                                                {{--<li><a href="#">Settings 2</a>--}}
                                                {{--</li>--}}
                                            {{--</ul>--}}
                                        {{--</li>--}}
                                        {{--<li><a href="#"><i class="fa fa-close"></i></a>--}}
                                        {{--</li>--}}
                                    {{--</ul>--}}
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <table id="example" class="table table-striped responsive-utilities jambo_table">
                                        <thead>
                                            <tr class="headings">
                                                <th>
                                                    <input type="checkbox" class="tableflat">
                                                </th>
                                                <th>ID </th>
                                                <th>Requisition </th>
                                                <th>Description </th>
                                                <th>Requisition By </th>
                                                <th>Priority </th>
                                                <th>Updated By </th>
                                                <th>Status </th>



                                                <th class=" no-link last"><span class="nobr">Action</span>
                                                </th>
                                            </tr>
                                        </thead>


                                        <tbody>
                                        @if($requestlist != null)
                                        @foreach($requestlist as $requisition)
                                            <tr class="even pointer">
                                                <td class="a-center ">
                                                    <input type="checkbox" class="tableflat">
                                                </td>
                                                <td class=" ">{{$requisition->taskid}}</td>
                                                <td class=" ">{{$requisition->categorie_name }}</td>
                                                <td class=" ">{{$requisition->task_des}}</td>
                                                <td class=" ">{{$requisition->email}}</td>
                                                <td class=" ">{{$requisition->task_priority}}</td>
                                                <td class=" ">@if( $requisition->updated_by > 0){{\App\Task::get_updated_by($requisition->updated_by)}}@else - @endif
                                                </td>
                                                <td class=" ">{{$requisition->status}}</td>
                                                <td class=" last">
                                                    {{--{{print_r($user)}}--}}
                                                    @if($user->user_type == 0 && $requisition->statusId == 0 || $requisition->statusId == 3 )
                                                        <a href="{{URL::to('reqformupdate')}}/{{$requisition->taskid}}"><i class="fa fa-edit green" style="font-size:20px;"></i></a>
                                                    @elseif($user->user_type == 1 && $requisition->statusId == 0)
                                                        <a href="{{URL::to('reqformupdate')}}/{{$requisition->taskid}}"><i class="fa fa-edit green" style="font-size:20px;"></i></a>
                                                    @elseif($user->user_type == 2 )
                                                        <a href="{{URL::to('reqformupdate')}}/{{$requisition->taskid}}"><i class="fa fa-edit green" style="font-size:20px;"></i></a>
                                                    @elseif($user->user_type == 3)
                                                        <a href="{{URL::to('reqformupdate')}}/{{$requisition->taskid}}"><i class="fa fa-edit green" style="font-size:20px;"></i></a>
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                            @endif
                                        </tbody>


                                    </table>
                                    @if($requestlist != null)
                                    {{$requestlist->render()}}
                                        @endif
                                </div>
                            </div>
                        </div>



                    </div>
                </div>
@include('partials.footer')