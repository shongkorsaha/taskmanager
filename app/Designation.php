<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Designation extends Model
{
    public static function store($data){
        return   DB::table('designations')->insertGetId(
            [
                'designation_name'     => $data['degName'],
                'designation_des'      => $data['degDetails']

            ]

        );

    }

    public static function showdeg(){
        return DB::table('designations')->get();
    }

    //show designation form for update
    public static function showDegById($id){
        return DB::table('designations')
            ->where('id',$id)
            ->get();
    }

    public static function updateDeg($data){
        return
            DB::table('designations')
                ->where('id', $data['id'])
                ->update([
                    'designation_name' => $data['designation_name'],
                    'designation_des'  => $data['designation_des']
                ]);
    }
    //delete department by id
    public static function destroyDegById($request){
        return DB::table('designations')
            ->where('id', $request->id)
            ->delete();
    }
}
