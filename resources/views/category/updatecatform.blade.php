@include('partials.header')

        <!-- page content -->
<div class="right_col" role="main">
    <div class="">

        <div class="page-title">
            <div class="title_left">
                <h3>Form Elements</h3>
            </div>
            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Update Task Category </h2>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />

                        <form action="{{URL::to('/catupdate')}}/{{$category->id}}" method="post" enctype="multipart/form-data" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
                            <div class="form-group">
                                {{--{{URL::to('catupdate')}}/{{'profile->id'}}--}}
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Task Category : <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {{--<input type="hidden" class="form-control" name="id" value="{{$profile->id}}">--}}
                                    {{--{{print_r($category)}}--}}
                                    <input type="hidden" class="form-control" name="id" value="{{$category->id}}" >
                                    <input type="text" class="form-control" name="categorie_name" value="{{$category->categorie_name}}" >

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Task Description : <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text"  class="form-control" name="categorie_des" value="{{$category->categorie_des}}"   >
                                </div>
                            </div>
                            <div class="form-group">

                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Department : <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select class="form-control"  name="dept_id" required>
                                        <option > -- Select Department --</option>

                                        @foreach($dept as $department)
                                            <option
                                                    @if($department->id == $category->dept_id)
                                                    selected
                                                    @endif
                                                    value="{{$department->id}}">{{$department->department_name}}
                                            </option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>

                            <div class="ln_solid"></div>
                            {{ csrf_field() }}
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">

                                    <button type="submit" name="updateUser" class="btn btn-success">Update</button>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>


@include('partials.footerForm')