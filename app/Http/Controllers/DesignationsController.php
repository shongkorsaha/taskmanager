<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Designation;

class DesignationsController extends Controller
{
    public $user;
    function __construct(Request $request)
    {
        if(!$request->session()->has('logininfo')){
            return redirect('/')->with('error','You have to Login first');
        }

        $data =  $request->session()->get('logininfo');
        $this->user = $data[0];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('designation.degadd',['user'=>$this->user]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'degName'      => 'required |max:255',
            'degDetails'      => 'required |max:255',
        ));
        $data = $request->all();
        $designations = Designation::store($data);
        //dd($designations);
        return redirect('newdeg')->with('success','Your request has been submitted successfully.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $deg = Designation::showdeg();

        return view('designation.showdeg',['user'=>$this->user,'designations'=>$deg]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editform($id)
    {
        $deg = Designation::showDegById($id);
       // dd($deg);

        return view('designation.updatedegform',['user'=>$this->user, 'deg'=>$deg[0]]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->all();
        $update =Designation::updateDeg($data);
        return redirect('alldeg' )->with('success','Data Updated Successfully!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        $deleteDept = Designation::destroyDegById($request);
        return redirect('alldeg')->with('success','Delete successfully.');
    }
}
