<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('task_name');
            $table->string('task_des');
            $table->string('photo',4);
            $table->timestamp('task_created_at');
            $table->timestamp('task_closed_at');
            $table->timestamp('task_updated_at');
            $table->integer('updated_by');
            $table->integer('assigned_id');

            $table->integer('status_id')->unsigned()->index();
            //$table->foreign('status_id')->references('id')->on('statuses');

            $table->integer('user_id');
            $table->integer('user_dept');
            $table->integer('category_id');
            $table->integer('priority_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tasks');
    }
}
