@extends('layouts.master')

@section('title')

    <title>Edit Wedget</title>

@endsection

@section('content')


        <ol class='breadcrumb'>
        <li><a href='/'>Home</a></li>
        <li><a href='/wedget'>Wedgets</a></li>
        <li><a href='/wedget/{{$wedget->id}}'>{{$wedget->wedget_name}}</a></li>
        <li class='active'>Edit</li>
        </ol>

        <h1>Edit Wedget</h1>

        <hr/>


        <form class="form" role="form" method="POST" action="{{ url('/wedget/'. $wedget->id) }}">
        <input type="hidden" name="_method" value="patch">
        {!! csrf_field() !!}

        <!-- wedget_name Form Input -->
            <div class="form-group{{ $errors->has('wedget_name') ? ' has-error' : '' }}">
                <label class="control-label">Wedget Name</label>

                    <input type="text" class="form-control" name="wedget_name" value="{{ $wedget->wedget_name }}">

                    @if ($errors->has('wedget_name'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('wedget_name') }}</strong>
                                    </span>
                    @endif

            </div>

            <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg">
                        Edit
                    </button>
            </div>

        </form>


@endsection