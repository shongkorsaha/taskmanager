
        <!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> E-TaskManagementSystem </title>

    <link rel="stylesheet" href="{{ URL::asset('css/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('resources/demos/style.css') }}">
    <!-- Bootstrap core CSS  -->

    <link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">


    {{--{!! Html::style('css/parsley.css') !!}--}}
    <link href="{{ URL::asset('fonts/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/animate.min.css') }}" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="{{ URL::asset('css/custom.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/maps/jquery-jvectormap-2.0.1.css') }}" />
    <link href="{{ URL::asset('css/icheck/flat/green.css" rel="stylesheet') }}" />
    <link href="{{ URL::asset('css/floatexamples.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('css/datatables/tools/css/dataTables.tableTools.css') }}" rel="stylesheet">

    {{--{!! Html::script('js/bootstrap.min.js') !!}--}}
    {{--<script src="{{ URL::asset('js/jquery.min.js') }}"></script>--}}
    <script src="{{ URL::asset('js/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('js/nprogress.js') }}"></script>
    <script>
        NProgress.start();
    </script>

    <!--[if lt IE 9]>
    <script src="{{ URL::asset('assets/js/ie8-responsive-file-warning.js') }}"></script>
    <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="{{ URL::asset('js/html5shiv.min.js') }}"></script>
    <script src="{{ URL::asset('js/respond.min.js') }}"></script>
    <![endif]-->

    <script src="{{ URL::asset('js/jquery-1.12.4.js') }}"></script>
    <script src="{{ URL::asset('js/jquery-ui.js') }}"></script>

    <script src="{{ URL::asset('css/util.css') }}"></script>
    <script src="{{ URL::asset('css/tooltip.css') }}"></script>
    {{--barchatt--}}
    <script type="text/javascript" src="{{ URL::asset('js/loader.js') }}"></script>


    <style>
        .DTTT_container{
            display:none;

        }
    </style>
</head>


<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <div class="navbar nav_title" style="border: 0;">
                    <a href="{{ URL::to('dashboard') }}" class="site_title"><i class="fa fa-paw"></i> <span>E-TaskManager</span></a>
                </div>
                <div class="clearfix"></div>

                <!-- menu prile quick info -->
                <div class="profile">
                    <div class="profile_pic">
                        <img src="{{ URL::to('/photos/'.$user->id.'.'.$user->photo) }}" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                        <span>Welcome,</span>
                        <h2>{{$user->full_name}}</h2>
                    </div>
                </div>
                <!-- /menu prile quick info -->

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                    <div class="menu_section">
                        <h3>General</h3>
                        <ul class="nav side-menu">
                            <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu" style="display: none">
                                    <li><a href="{{ URL::to('dashboard') }}">Dashboard</a>
                                    </li>

                                </ul>
                            </li>
                            <li><a><i class="fa fa-edit "></i> Requisition  <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu" style="display: none">
                                    <li><a href="{{ URL::to('reqform') }}">New Requisition</a>
                                    </li>
                                    <li><a href="{{ URL::to('allreq') }}">Requisition List</a>
                                    </li>

                                </ul>
                            </li>
                            @if($user->user_type > 1 )
                                <li><a><i class="fa fa-edit"></i> Task Category <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="{{ URL::to('catform') }}">Add Category</a>
                                        </li>
                                        <li><a href="{{ URL::to('allcat') }}">All Caterory</a>
                                        </li>

                                    </ul>

                                </li>
                            @endif

                            @if($user->user_type > 1 )
                                <li><a><i class="fa fa-edit"></i> Departments <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="{{ URL::to('newdept') }}">Add Department</a>
                                        </li>
                                        <li><a href="{{ URL::to('alldept') }}">All Department</a>
                                        </li>

                                    </ul>
                                </li>
                            @endif
                            @if($user->user_type > 1 )
                                <li><a><i class="fa fa-edit"></i> Designations <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="{{ URL::to('newdeg') }}">New Designation</a>
                                        </li>
                                        <li><a href="{{ URL::to('alldeg') }}">All Designation</a>
                                        </li>
                                    </ul>
                                </li>
                            @endif

                            <li><a><i class="fa fa-user"></i> User <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu" style="display: none">
                                    @if($user->user_type > 1 )
                                        <li><a href="{{ URL::to('newuserform') }}">New User</a>
                                        </li>
                                    @endif
                                    <li><a href="{{ URL::to('alluser') }}">All User</a>
                                    </li>

                                </ul>
                            </li>



                        </ul>
                    </div>


                </div>

            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">

            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="{{ URL::to('/photos/'.$user->id.'.'.$user->photo) }}" alt="">{{ $user->full_name }}
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                <li><a href="javascript:;">  Profile</a>
                                </li>


                                <li><a href="{{URL::to('/')}}"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                </li>
                            </ul>
                        </li>

                        <li role="presentation" class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-envelope-o"></i>
                                <span class="badge bg-green">Pending</span>
                            </a>
                            <ul id="menu1" class="dropdown-menu list-unstyled msg_list animated fadeInDown" role="menu">

                            </ul>
                        </li>

                    </ul>
                </nav>
            </div>

        </div>
        <!-- /top navigation -->